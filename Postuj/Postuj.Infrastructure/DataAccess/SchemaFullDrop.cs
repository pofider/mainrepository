﻿namespace Postuj.Infrastructure.DataAccess
{
    /// <summary>
    /// NHibernate neumi spravne dropnout schema, resime to hrubou silou pomoci drop scriptu CleanUpSchema.sql
    /// </summary>
    public class SchemaFullDrop
    {
        private readonly IDatabaseFacade _databaseFacade;

        public SchemaFullDrop(IDatabaseFacade databaseFacade)
        {
            _databaseFacade = databaseFacade;
        }

        public void DropFull()
        {
            _databaseFacade.ExecuteEmbededScript("Postuj.Infrastructure.DataAccess.SQL.CleanUpSchema.sql",
                GetType().Assembly, 360);
        }
    }
}