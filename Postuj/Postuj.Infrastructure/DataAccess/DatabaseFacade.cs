﻿using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace Postuj.Infrastructure.DataAccess
{
    public class DatabaseFacade : IDatabaseFacade
    {
        private readonly string _connectionString;

        public DatabaseFacade(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int ExecuteEmbededScript(string manifestName, Assembly assembly, int timeOut, params string[] scriptParams)
        {
            var scriptStream = assembly.GetManifestResourceStream(manifestName);

            var script = new StreamReader(scriptStream).ReadToEnd();

            return ExecuteNonQuery(string.Format(script, scriptParams), timeOut);
        }

        public bool IsMsSqlInEnterpriseEdition()
        {
            return GetMsSqlServerEdition().StartsWith("Enterprise");
        }

        public bool IsMsSqlInStandardEdition()
        {
            return GetMsSqlServerEdition().StartsWith("Standard");
        }

        public int ExecuteNonQuery(string commandText)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var nonQueryCommand = connection.CreateCommand();
                nonQueryCommand.CommandText = commandText;

                connection.Open();
                return nonQueryCommand.ExecuteNonQuery();
            }
        }

        public int ExecuteNonQuery(string commandText, int timeOut)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var nonQueryCommand = connection.CreateCommand();
                nonQueryCommand.CommandText = commandText;
                nonQueryCommand.CommandTimeout = timeOut;

                connection.Open();
                return nonQueryCommand.ExecuteNonQuery();
            }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        private string GetMsSqlServerEdition()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();
                command.CommandText = "SELECT SERVERPROPERTY ('edition')";

                connection.Open();

                return (string)command.ExecuteScalar();
            }
        }
    }
}