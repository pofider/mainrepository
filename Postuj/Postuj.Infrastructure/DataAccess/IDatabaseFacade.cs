﻿using System.Reflection;

namespace Postuj.Infrastructure.DataAccess
{
    public interface IDatabaseFacade
    {
        int ExecuteNonQuery(string commandText);
        int ExecuteNonQuery(string commandText, int timeOut);
        int ExecuteEmbededScript(string manifestName, Assembly assembly, int timeOut, params string[] scriptParams);
        bool IsMsSqlInEnterpriseEdition();
        bool IsMsSqlInStandardEdition();

        string ConnectionString { get; }
    }
}