﻿using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Event;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using Postuj.Infrastructure.DependencyInjection;
using log4net;
using Configuration = NHibernate.Cfg.Configuration;

namespace Postuj.Infrastructure.DataAccess.ORM
{
    public class OrmConfigure
    {
        private Assembly _assembly;
        private bool _createSchema;
        private bool _sessionPerDependency;

        private static ILog _log = LogManager.GetLogger(typeof (OrmConfigure));

        public OrmConfigure ForAssemblyType<T>()
        {
            _assembly = typeof (T).Assembly;
            return this;
        }

        public OrmConfigure UpdateSchema()
        {
            _createSchema = false;
            return this;
        }

        public OrmConfigure CreateSchema()
        {
            _createSchema = true;
            return this;
        }

        public OrmConfigure WithSessionPerDependency()
        {
            _sessionPerDependency = true;
            return this;
        }

        public ISessionFactory ConfiguraOrm()
        {
            var databaseFacade = new DatabaseFacade(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Connection"]].ConnectionString);
            ContainerBuilderProvider.Builder.RegisterInstance(databaseFacade).As<IDatabaseFacade>().SingleInstance();

            ISessionFactory sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                              .ConnectionString(
                                  c => c.FromConnectionStringWithKey(ConfigurationManager.AppSettings["Connection"]))).Mappings(
                                      m => m.FluentMappings.AddFromAssembly(_assembly)).ExposeConfiguration((cfg) =>
                                         {
                                             UpdateManyToManyMappingsForClusteredIndex(cfg);

                                             if (_createSchema)
                                             {
                                                 new SchemaFullDrop(databaseFacade).DropFull();
                                                 new SchemaExport(cfg).Execute(false, true, false);
                                             }
                                             else
                                                 new SchemaUpdate(cfg).Execute(false, true);
                                         })
                  .BuildSessionFactory();

            ContainerBuilderProvider.Builder.RegisterInstance(new SessionProvider(sessionFactory)).As<ISessionProvider>();

            ContainerBuilderProvider.Builder.RegisterInstance(sessionFactory).As<ISessionFactory>().SingleInstance();

            if (_sessionPerDependency)
                ContainerBuilderProvider.Builder.Register(s => s.Resolve<ISessionFactory>().OpenSession()).As<ISession>().InstancePerDependency();
            else
                ContainerBuilderProvider.Builder.Register(OpenSessionAndTransaction).As<ISession>().InstancePerHttpRequest().OnRelease(CommitOrRollbackTransaction);

            return sessionFactory;
        }

        private void UpdateManyToManyMappingsForClusteredIndex(Configuration cfg)
        {
            foreach (var collectionMapping in cfg.CollectionMappings.Where(x => !x.IsOneToMany))
            {
                // Build the columns (in a hacky way...) 
                const string columnFormat = "{0}_id";
                var leftColumn = new Column(string.Format(
                    columnFormat,
                    collectionMapping.Owner.MappedClass.Name));
                var rightColumn = new Column(string.Format(
                    columnFormat,
                    collectionMapping.GenericArguments[0].Name));
                // Fetch the actual table of the many-to-many collection 
                var manyToManyTable = collectionMapping.CollectionTable;
                // Shorten the name just like NHibernate does 
                var shortTableName = (manyToManyTable.Name.Length <= 8)
                                            ? manyToManyTable.Name
                                            : manyToManyTable.Name.Substring(0, 8);
                // Create the primary key and add the columns 
                var primaryKey = new PrimaryKey
                {
                    Name = string.Format("PK_{0}", shortTableName),
                };
                primaryKey.AddColumn(leftColumn);
                primaryKey.AddColumn(rightColumn);
                // Set the primary key to the junction table 
                manyToManyTable.PrimaryKey = primaryKey;
            } 
        }

        private static bool ValidateSchema(Configuration cfg)
        {
            var myvalidator = new SchemaValidator(cfg);
            try
            {
                myvalidator.Validate();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void CommitOrRollbackTransaction(ISession session)
        {
            if (HttpContext.Current.AllErrors != null && HttpContext.Current.AllErrors.Length > 1)
            {
                try
                {
                    _log.Error("Error when processing request " + HttpContext.Current.Request.RawUrl, HttpContext.Current.Error);
                    session.Transaction.Rollback();
                }
                finally
                {
                    session.Transaction.Dispose();
                }
                return;
            }

            try
            {
                session.Transaction.Commit();
            }
            finally
            {
                session.Transaction.Dispose();
            }
        }

        private static ISession OpenSessionAndTransaction(IComponentContext x)
        {
            ISession session = x.Resolve<ISessionFactory>().OpenSession();
            session.BeginTransaction(IsolationLevel.ReadCommitted);
            return session;
        }
    }
}