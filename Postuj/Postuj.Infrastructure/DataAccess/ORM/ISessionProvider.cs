﻿using NHibernate;

namespace Postuj.Infrastructure.DataAccess.ORM
{
    public interface ISessionProvider
    {
        ISession PerThreadSession();
        void TryRollbackAndDispose();
        void TryCommitAndDispose();
    }
}