﻿using FluentNHibernate.Mapping;

namespace Postuj.Infrastructure.DataAccess.ORM
{
    public class EntityMapBase<TEntity> : ClassMap<TEntity> where TEntity : EntityBase
    {
        public EntityMapBase()
        {
            Id(x => x.Id).GeneratedBy.Identity();
        }
    }
}