﻿

using System;

namespace Postuj.Infrastructure.DataAccess.ORM
{
    [Serializable]
    public abstract class EntityBase
    {
        public virtual long? Id { get; protected set; }

        private int? _transientHashCode;

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        public override string ToString()
        {
            return GetType().Name + " = "
                   + (IsTransient() ? "(Transient)" : Id.ToString());
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// If persistent, uses Id; if transient, uses temporary code.
        /// (This will not work if the entity is evicted and reloaded)
        /// </summary>
        public override int GetHashCode()
        {
            if (_transientHashCode.HasValue) return _transientHashCode.Value;
            if (IsTransient())
            {
                _transientHashCode = base.GetHashCode();
                return _transientHashCode.Value;
            }
            return Id.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        public override bool Equals(object obj)
        {
            var x = obj as EntityBase;
            if (x == null) return false;
            if (IsTransient() && x.IsTransient()) return ReferenceEquals(this, x);
            return (Id.Equals(x.Id));
        }

        /// <summary>
        /// Implements the operator == (Equals).
        /// </summary>
        public static bool operator ==(EntityBase first, EntityBase second)
        {
            return Equals(first, second);
        }

        /// <summary>
        /// Implements the operator != (Not Equals).
        /// </summary>
        public static bool operator !=(EntityBase first, EntityBase second)
        {
            return !(Equals(first, second));
        }

        /// <summary>
        /// Determines whether this instance is transient.
        /// </summary>
        public virtual bool IsTransient()
        {
            return Id == null;
        }
    }
}