﻿using System.Threading;
using NHibernate;

namespace Postuj.Infrastructure.DataAccess.ORM
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ThreadLocal<ISession> _session = new ThreadLocal<ISession>();
        private static readonly object _locker = new object();
        private readonly ISessionFactory _sessionFactory;

        public SessionProvider(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public ISession PerThreadSession()
        {
            lock (_locker)
            {
                if (_session.Value == null)
                {
                    _session.Value = _sessionFactory.OpenSession();
                    _session.Value.BeginTransaction();
                }
            }

            return _session.Value;
        }

        public void TryRollbackAndDispose()
        {
            if (_session.Value != null)
            {
                try
                {
                    if (_session.Value.Transaction.IsActive)
                        _session.Value.Transaction.Rollback();
                }
                finally
                {
                    _session.Value.Close();
                    _session.Dispose();
                }
                
            }
        }

        public void TryCommitAndDispose()
        {
            if (_session.Value != null)
            {
                try
                {
                    if (_session.Value.Transaction.IsActive)
                        _session.Value.Transaction.Commit();
                }
                finally
                {
                    _session.Value.Close();
                    _session.Dispose();
                }

            }
        }
    }
}