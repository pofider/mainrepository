﻿using System.Configuration;
using Autofac;
using BitlyDotNET.Implementations;
using BitlyDotNET.Interfaces;
using MFR.Infrastructure.FileSystem;
using Postuj.Infrastructure.Cache;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.FileSystem;
using Postuj.Infrastructure.Logging;
using Postuj.Infrastructure.Query;

namespace Postuj.Infrastructure
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QueryExecutor>().As<IQueryExecutor>();
            builder.RegisterType<CommandExecutor>().As<ICommandExecutor>();

            builder.RegisterType<SimpleCache>().As<ICache>().SingleInstance();

            builder.RegisterInstance(new BitlyService("pofider", "R_107726339a3b6bc1c3a073a5cddfa825")).As<IBitlyService>().SingleInstance();

            if (UseFileSystem())
                builder.RegisterType<FileSystemStorageProvider>().As<IStorageProvider>();
            else
            builder.RegisterType<AzureBlobStorageProvider>().As<IStorageProvider>();
        }

        private bool UseFileSystem()
        {
            var settings = ConfigurationManager.AppSettings["UseFileSystem"];

            bool useSettings;
            if (!bool.TryParse(settings, out useSettings))
                return false;

            return useSettings;
        }

    }
}