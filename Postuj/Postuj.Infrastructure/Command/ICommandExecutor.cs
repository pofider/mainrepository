﻿namespace Postuj.Infrastructure.Command
{
    public interface ICommandExecutor
    {
        ICommandResult<TResult> Execute<TResult>(IGenericCommand<TResult> coomand, ICommand parrentCommand = null);

        void Execute(ICommand coomand, ICommand parrentCommand = null);
    }
}