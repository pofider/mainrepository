﻿using System.Collections.Generic;

namespace Postuj.Infrastructure.Command
{
    public interface ICommandError
    {
        IList<string> Messages { get; }
        string FormatedMessage { get; }
        bool HasErrors { get; }

        void AddInnerError(ICommandError inner);
    }
}