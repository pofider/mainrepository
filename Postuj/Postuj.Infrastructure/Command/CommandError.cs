﻿using System.Collections.Generic;
using System.Linq;

namespace Postuj.Infrastructure.Command
{
    public class CommandError : ICommandError
    {
        public CommandError()
        {
            Messages = new List<string>();
        }

        public CommandError(string message) : this()
        {
            Messages.Add(message);
        }

        public IList<string> Messages { get; private set; }

        public string FormatedMessage
        {
            get { return string.Join(" ", Messages); }
        }

        public bool HasErrors
        {
            get { return Messages.Any(); }
        }

        public void AddInnerError(ICommandError inner)
        {
            foreach (var msg in inner.Messages)
            {
                Messages.Add(msg);    
            }
        }
    }
}