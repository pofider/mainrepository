﻿namespace Postuj.Infrastructure.Command
{
    public abstract class GenericCommandBase<TResult> : CommandBase, IGenericCommand<TResult>
    {
        protected GenericCommandBase()
        {
            Result = new CommandResult<TResult>();
        }

        public ICommandResult<TResult> Result { get; set; }

        protected void Success(TResult result)
        {
            Result.Result = result;
        }

        protected void Fail(string message)
        {
            Result.Error.Messages.Add(message);
        }

        protected void Fail(ICommandError innerError)
        {
            Result.Error.AddInnerError(innerError);
        }
    }
}