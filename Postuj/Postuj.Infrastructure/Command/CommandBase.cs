﻿using System.Collections.Generic;
using System.Web;
using NHibernate;
using Postuj.Infrastructure.Cache;
using Postuj.Infrastructure.Query;
using Postuj.Infrastructure.Security;

namespace Postuj.Infrastructure.Command
{
    public abstract class CommandBase : ICommand
    {
        public void Execute()
        {
            ExecuteInternal();
        }

        public ISession Session { get; set; }
        public ICommandExecutor CommandExecutor { get; set; }
        public IQueryExecutor QueryExecutor { get; set; }
        public ICommand ParrentCommand { get; set; }
        public ICache Cache { get; set; }
        private IUser _fakeUser;

        public IUser CurrentUser
        {
            get { return (ParrentCommand != null) ? ((CommandBase)ParrentCommand).CurrentUser : _fakeUser ?? HttpContext.Current.User.Identity as IUser; }
            set { _fakeUser = value; }
        }

        protected abstract void ExecuteInternal();

        protected void ExecuteCommand(ICommand command)
        {
            CommandExecutor.Execute(command, this);
        }

        protected ICommandResult<TResult> ExecuteCommand<TResult>(IGenericCommand<TResult> command)
        {
            return CommandExecutor.Execute(command, this);
        }

        protected IEnumerable<TResult> Query<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.Execute(query);
        }

        protected TResult QuerySingle<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.ExecuteSingle(query);
        }
    }
}