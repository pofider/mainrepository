﻿namespace Postuj.Infrastructure.Command
{
    public class CommandResult<TResult> : ICommandResult<TResult>
    {
        public CommandResult()
        {
            Error = new CommandError();
        }

        public CommandResult(TResult result)
            : this()
        {
            Result = result;
        }

        public CommandResult(ICommandError error)
            : this()
        {
            Error = error;
        }

        public TResult Result { get; set; }
        public ICommandError Error { get; set; }

        public bool IsSuccess
        {
            get { return !Error.HasErrors; }
        }
    }
}