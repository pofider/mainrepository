﻿using System;
using Autofac;
using NHibernate;
using Postuj.Infrastructure.Logging;
using Postuj.Infrastructure.Query;
using log4net;

namespace Postuj.Infrastructure.Command
{
    public class CommandExecutor : ICommandExecutor
    {
        private readonly ISession _session;
        private readonly IComponentContext _container;
        private readonly IQueryExecutor _queryExecutor;
        public ILog Log { get; set; }

        public CommandExecutor(ISession session, IComponentContext container, IQueryExecutor queryExecutor)
        {
            _session = session;
            _container = container;
            _queryExecutor = queryExecutor;
            Log = LogManager.GetLogger(GetType());
        }

        public virtual ICommandResult<TResult> Execute<TResult>(IGenericCommand<TResult> command, ICommand parrentCommand = null)
        {
            PrepareCommand(command, parrentCommand);

            ExecuteCommand(command);

            return command.Result;
        }

        public virtual void Execute(ICommand command, ICommand parrentCommand = null)
        {
            PrepareCommand(command, parrentCommand);

            ExecuteCommand(command);
        }

        private void ExecuteCommand(ICommand command)
        {
            try
            {
                command.Execute();
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Error occured when executing command {0}.", command.GetType().Name), e);
                throw;
            }
        }

        private void PrepareCommand(ICommand command, ICommand parrentCommand)
        {
            Log.Info(string.Format("Executing command {0}.", command.GetType().Name));
            command.Session = _session;
            command.CommandExecutor = this;
            command.QueryExecutor = _queryExecutor;
            command.ParrentCommand = parrentCommand;

            if (_container != null)
                _container.InjectUnsetProperties(command);
        }
    }
}