﻿namespace Postuj.Infrastructure.Command
{
    public interface ICommandResult<TResult>
    {
        TResult Result { get; set; }
        ICommandError Error { get;  }
        
        bool IsSuccess { get; }
    }
}