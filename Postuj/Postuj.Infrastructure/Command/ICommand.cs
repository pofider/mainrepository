﻿using NHibernate;
using Postuj.Infrastructure.Cache;
using Postuj.Infrastructure.Query;
using Postuj.Infrastructure.Security;

namespace Postuj.Infrastructure.Command
{
    public interface ICommand
    {
        void Execute();

        ISession Session { set; }
        ICommandExecutor CommandExecutor { set; }
        IQueryExecutor QueryExecutor { set; }
        ICommand ParrentCommand { get; set; }
        IUser CurrentUser { get; set; }
        ICache Cache { get; set; }
    }

    public interface IGenericCommand<TResult> : ICommand
    {
        ICommandResult<TResult> Result { get; set; }
    }
}