﻿using System.Collections.Generic;
using System.Web.Mvc;
using NHibernate;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Query;

namespace Postuj.Infrastructure.Common
{
    public class PostujControllerBase : Controller
    {
        protected new ISession Session { get; private set; }
        protected IQueryExecutor QueryExecutor { get; private set; }
        public ICommandExecutor CommandExecutor { get; private set; }

        public PostujControllerBase(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            Session = session;
            QueryExecutor = queryExecutor;
            CommandExecutor = commandExecutor;
        }

        protected PostujControllerBase()
        {

        }

        protected IEnumerable<TResult> Query<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.Execute(query);
        }

        protected TResult Command<TResult>(IGenericCommand<TResult> command)
        {
            return CommandExecutor.Execute(command).Result;
        }

        protected void Command(ICommand command)
        {
            CommandExecutor.Execute(command);
        }

    }
}