﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BitlyDotNET.Implementations;
using BitlyDotNET.Interfaces;

namespace Postuj.Infrastructure.Common
{
    public static class StringExtensions
    {
        public static string ReplaceDiacritics(this string str)
        {
            var czechChars =  "ěščřžýáíéóúů";
            var normalChars = "escrzyaieouu";

            str = str.ToLower();
            for (int i = 0; i < czechChars.Length; i++)
            {
                str = str.Replace(czechChars[i], normalChars[i]);
            }

            return str;
        }

        public static string Shorten(this string str)
        {
            return str.Length < 33 ? str : str.Substring(0, 30) + "...";
        }

        public static string FormatUrls(this string input)
        {
            string output = input;
            Regex regx = new Regex("(http(s)?://)?([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*([a-zA-Z0-9\\?\\#\\=\\/]){1})?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(output);

            var index = 0;
            foreach (Match match in mactches)
            {
                if (!match.Value.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase) &&
                    !match.Value.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase) &&
                    !match.Value.StartsWith("www.", StringComparison.InvariantCultureIgnoreCase))
                    continue;

                var url = (match.Value.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase) || match.Value.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
                              ? match.Value
                              : "http://" + match.Value;


                output = output.Remove(match.Index + index, match.Length);
                var replaceText = "<a href='" + url + "' target='blank'>" + match.Value + "</a>";
                output = output.Insert(match.Index + index, replaceText);

                index += replaceText.Length - match.Length;
            }

            return output;
        }

        public static string ShortenUrls(this string input, IBitlyService service)
        {
            Regex regx = new Regex(@"((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(input);
            var output = input;
            foreach (Match match in mactches)
            {
                string shortened;
                if (service.Shorten(match.Value, out shortened) == StatusCode.OK)
                    output = output.Replace(match.Value, shortened);
            }

            return output;
        }

        public static bool ContainsAny(this string str, string[] filters)
        {
            return filters.Any(str.Contains);
        }
    }
}
