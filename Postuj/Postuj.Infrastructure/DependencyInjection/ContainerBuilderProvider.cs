﻿using Autofac;

namespace Postuj.Infrastructure.DependencyInjection
{
    public class ContainerBuilderProvider
    {
        private volatile static ContainerBuilder _containerBuilder;
        private static readonly object _locker = new object();

        public static ContainerBuilder Builder
        {
            get
            {
                if (_containerBuilder == null)
                {
                    lock (_locker)
                    {
                        //yeah i am serious, double check pattern

                        if (_containerBuilder == null)
                            _containerBuilder = new ContainerBuilder();
                    }
                }

                return _containerBuilder;
            }
        } 
    }
}