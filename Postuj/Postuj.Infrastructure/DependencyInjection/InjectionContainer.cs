﻿using System;
using Autofac;

namespace Postuj.Infrastructure.DependencyInjection
{
    public class InjectionContainer
    {
        private static IContainer _container;
        private static readonly object _locker = new object();

        public static IContainer Instance
        {
            set
            {
                lock (_locker)
                {
                    if (_container != null)
                        throw new InvalidOperationException("DI InjectionContainer was already set.");

                    _container = value;
                }
            }
            get { return _container; }
        }  
    }
}