﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.WindowsAzure.ServiceRuntime;
using log4net.Appender;

namespace Postuj.Infrastructure.Logging
{
    public class AzureLocalStorageAppender : RollingFileAppender  
    {  
        public override string File  
        {  
            set  
            {  
                base.File = RoleEnvironment.GetLocalResource("Log4Net").RootPath + @"\"  
                    + new FileInfo(value).Name + "_"  
                    + Guid.NewGuid().ToString().Substring(0, 4);  
            }  
        }  
    }  

}