﻿using System.Web;

namespace Postuj.Infrastructure.Logging
{
    public class HttpContextUserNameProvider
    {
        public override string ToString()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.User != null)
            {
                return context.User.Identity.Name;
            }
            return "";
        } 
    }
}