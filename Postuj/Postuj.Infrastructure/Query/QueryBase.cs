﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using Autofac;
using NHibernate;
using System.Linq;
using Postuj.Infrastructure.Cache;
using Postuj.Infrastructure.Security;
using log4net;

namespace Postuj.Infrastructure.Query
{
    public abstract class QueryBase<TResult> : IQuery<TResult>
    {
        protected QueryBase()
        {
            PageSize = 10;
        }

        public virtual IEnumerable<TResult> Invariant(IEnumerable<TResult> results)
        {
            return results;
        }

        public virtual TResult ExecuteSingle()
        {
            throw new InvalidOperationException("Single query not supported");
        }

        public virtual string CacheKey { get { return null; } }

        public ISession Session { get; set; }
        public int? PageNumber { get; set; }
        public int PageSize { get; set; }
        public ILog Log = LogManager.GetLogger(typeof (QueryBase<>));

        public virtual IEnumerable<TResult> Execute()
        {
            throw new InvalidOperationException("Single multi not supported");
        }

        public virtual IUser CurrentUser
        {
            get { return HttpContext.Current != null ? HttpContext.Current.User.Identity as IUser : null; }
            set { 
                if (HttpContext.Current != null) 
                 HttpContext.Current.User = value as IPrincipal; 
            }
        }

        public IComponentContext Container { get; set; }
        public IQueryExecutor QueryExecutor { get; set; }

        protected IEnumerable<TResult> Query<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.Execute(query);
        }

        protected TResult QuerySingle<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.ExecuteSingle(query);
        }

        protected  IEnumerable<TResult> WithPaging(IEnumerable<TResult> result)
        {
            if (PageNumber != null)
                return result.Skip(PageSize * (PageNumber.Value - 1)).Take(PageSize);

            return result;
        } 
    }
}