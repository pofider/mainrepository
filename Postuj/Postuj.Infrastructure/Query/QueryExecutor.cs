﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autofac;
using NHibernate;
using Postuj.Infrastructure.Cache;
using Postuj.Infrastructure.Logging;
using log4net;

namespace Postuj.Infrastructure.Query
{
    public class QueryExecutor : IQueryExecutor
    {
        private readonly ISession _session;
        private readonly IComponentContext _container;
        public ICache Cache { get; set; }
        public ILog Log { get; set; }

        public QueryExecutor(ISession session, IComponentContext container, ICache cache)
        {
            _session = session;
            _container = container;
            Cache = cache;
            Log = LogManager.GetLogger(GetType());
        }

        public virtual IEnumerable<TResult> Execute<TResult>(IQuery<TResult> query)
        {
            PrepareQuery(query);

            try
            {
                var cacheKey = query.CacheKey;

                if (cacheKey == null)
                    return query.Execute().ToList();

                var results = Cache.GetOrAdd(cacheKey, (s) => query.Execute().ToList())
                    as IEnumerable<TResult>;

                return query.Invariant(results);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Error occured when executing query {0}.", query.GetType().Name), e);
                throw;
            }
        }

        public virtual TResult ExecuteSingle<TResult>(IQuery<TResult> query)
        {
            PrepareQuery(query);

            try
            {
                return query.ExecuteSingle();
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Error occured when executing query {0}.", query.GetType().Name), e);
                throw;
            }
        }

        private void PrepareQuery<TResult>(IQuery<TResult> query)
        {
            Log.Info(string.Format("Executing query {0}.", query.GetType().Name));
            query.Session = _session;
            query.Container = _container;
            query.QueryExecutor = this;
        }
    }
}