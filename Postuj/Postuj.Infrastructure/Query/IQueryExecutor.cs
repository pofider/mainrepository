﻿using System.Collections.Generic;

namespace Postuj.Infrastructure.Query
{
    public interface IQueryExecutor
    {
        IEnumerable<TResult> Execute<TResult>(IQuery<TResult> query);

        TResult ExecuteSingle<TResult>(IQuery<TResult> query);
    }
}