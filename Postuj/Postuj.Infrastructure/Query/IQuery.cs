﻿using System.Collections.Generic;
using Autofac;
using NHibernate;
using Postuj.Infrastructure.Security;

namespace Postuj.Infrastructure.Query
{
    public interface IQuery<TResult>
    {
        ISession Session { get; set; }
        IEnumerable<TResult> Execute();
        IEnumerable<TResult> Invariant(IEnumerable<TResult> results);
        TResult ExecuteSingle();
        IComponentContext Container { get; set; }
        IQueryExecutor QueryExecutor { set; }
        IUser CurrentUser { get; set; }
        string CacheKey { get;  }
        int? PageNumber { get; set; }
        int PageSize { get; set; }
    }
}