﻿using System.Collections.Generic;
using NHibernate;

namespace Postuj.Infrastructure.Query
{
    public abstract class GenericQueryOverBase<TEntity, TResult> : QueryBase<TResult> where TEntity : class
    {
        public override IEnumerable<TResult> Execute()
        {
            var result = InnerExecute();
            return result;
        }

        protected abstract IEnumerable<TResult> InnerExecute();

     
        protected virtual IQueryOver<TEntity> WithPaging(IQueryOver<TEntity> entites)
        {
            if (PageNumber != null)
                return entites.Skip(PageSize * (PageNumber.Value - 1)).Take(PageSize);

            return entites;
        } 
    }
}