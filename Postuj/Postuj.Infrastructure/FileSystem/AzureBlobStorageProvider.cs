﻿using System.Configuration;
using System.IO;
using Microsoft.WindowsAzure;

namespace Postuj.Infrastructure.FileSystem
{
    public class AzureBlobStorageProvider : AzureFileSystem, IStorageProvider
    {
        public AzureBlobStorageProvider()//AzureBlobConnetion
            : this(CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureBlobConnection"].ConnectionString))
        {
        }

        public AzureBlobStorageProvider(CloudStorageAccount storageAccount)
            : base(storageAccount)
        {
        }
        
        public bool TrySaveStream(string path, Stream inputStream)
        {
            try
            {
                SaveStream(path, inputStream);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void SaveStream(string path, Stream inputStream)
        {
            // Create the file.
            // The CreateFile method will map the still relative path
            var file = CreateFile(path);

            using (var outputStream = file.OpenWrite())
            {
                var buffer = new byte[8192];
                for (; ; )
                {
                    var length = inputStream.Read(buffer, 0, buffer.Length);
                    if (length <= 0)
                        break;
                    outputStream.Write(buffer, 0, length);
                }
            }
        }

        public IStorageFile CreateOrReplaceFile(string path)
        {
            if (FileExists(path))
                DeleteFile(path);

            return CreateFile(path);
        }
    }
}