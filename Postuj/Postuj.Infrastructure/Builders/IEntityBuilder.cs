using NHibernate;

namespace MFR.Infrastructure.Builders
{
    public interface IEntityBuilder<TEntity>
    {
        TEntity Build();
        TEntity BuildAndWriteToDatabase(ISession session);
    }
}