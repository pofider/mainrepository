using System;
using System.Collections.Generic;
using System.Linq;

namespace MFR.Infrastructure.Builders
{
    public static class UniqueValueGenerator
    {
        [ThreadStatic]
        private static int _lastUsedInteger;
        [ThreadStatic]
        private static List<string> _alreadyUsedEnumValues;

        static UniqueValueGenerator()
        {
            Init();
        }

        public static int GetInt()
        {
            return ++_lastUsedInteger;
        }

        public static TEnumType GetEnum<TEnumType>()
        {
            IEnumerable<TEnumType> allValues = Enum.GetValues(typeof(TEnumType)).Cast<TEnumType>();

            foreach (TEnumType value in allValues)
            {
                string searchValue = CreateSearchValue<TEnumType>(value);

                if (_alreadyUsedEnumValues.Contains(searchValue) == false)
                {
                    _alreadyUsedEnumValues.Add(searchValue);
                    return value;
                }
            }
            throw new Exception(String.Format("I am out of range for unique values for enum {0}, sorry ", typeof(TEnumType)));
        }

        private static string CreateSearchValue<TEnumType>(TEnumType enumValue)
        {
            return typeof(TEnumType).ToString() + "_" + enumValue.ToString();
        }

        public static void Init()
        {
            _lastUsedInteger = 0;
            _alreadyUsedEnumValues = new List<string>();
        }
    }
}