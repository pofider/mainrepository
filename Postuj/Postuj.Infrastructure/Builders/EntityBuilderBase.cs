using NHibernate;

namespace MFR.Infrastructure.Builders
{
    public abstract class EntityBuilderBase<TEntity> : IEntityBuilder<TEntity>
    {
        public EntityBuilderBase(bool setupDefaultReferences = true)
        {
            if (setupDefaultReferences)
                SetUpDefaultReferences();
        }

        public TEntity Build()
        {
            return BuildEntity();
        }

        protected abstract TEntity BuildEntity();

        protected virtual void SetUpDefaultReferences()
        {
            
        }

        public TEntity BuildAndWriteToDatabase(ISession session)
        {
            var entity = Build();
            session.SaveOrUpdate(entity);
            return entity;
        }
    }
}