﻿using System;
using System.Collections.Concurrent;

namespace Postuj.Infrastructure.Cache
{
    public class SimpleCache : ICache
    {
        private readonly ConcurrentDictionary<string, object> _data = new ConcurrentDictionary<string, object>();  
        private readonly object _locker = new object();

        public object this[string key]
        {
            get
            {
                object value = null;
                return _data.TryGetValue(key, out value) ? value : null;
            }
            set { _data[key] = value; }
        }

        public object GetOrAdd(string key, Func<string, object> func)
        {
            return _data.GetOrAdd(key, func);
        }

        public void Clear()
        {
            lock (_locker)
            {
                _data.Clear();
            }
        }
    }
}