﻿using System;

namespace Postuj.Infrastructure.Cache
{
    public interface ICache
    {
        object this[string key] { get; set; }

        object GetOrAdd(string key, Func<string, object> func);

        void Clear();
    }
}