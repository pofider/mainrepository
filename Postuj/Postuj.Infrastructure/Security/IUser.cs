﻿namespace Postuj.Infrastructure.Security
{
    public interface IUser
    {
        long? Id { get; }
        bool IsAdmin { get; }
    }
}