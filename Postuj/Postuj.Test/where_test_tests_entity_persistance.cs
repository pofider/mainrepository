﻿using NUnit.Framework;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Test
{
    [TestFixture]
    public abstract class where_test_tests_entity_persistance<TEntity> : where_test_access_database where TEntity : EntityBase
    {
        protected abstract TEntity CreateAndSaveEntity();

        protected TEntity SavedEntity { get; private set; }
        protected TEntity LoadEntity { get; private set; }

        public void SaveAndLoad()
        {
            SavedEntity = CreateAndSaveEntity();

            Session.Flush();
            Session.Evict(SavedEntity);

            LoadEntity = Session.Load<TEntity>(SavedEntity.Id);
        }
    }
}
