﻿using Autofac;
using MFR.Infrastructure.Builders;
using NHibernate;
using NUnit.Framework;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;
using Postuj.Infrastructure.DependencyInjection;

namespace Postuj.Test
{
    public class where_smoke_test_access_database
    {
        protected ISession Session { get; private set; }

        [SetUp]
        public void TestSetUp()
        {
               new OrmConfigure()
                    .ForAssemblyType<Post>()
                    .CreateSchema()
                    .WithSessionPerDependency()
                    .ConfiguraOrm();

            if (InjectionContainer.Instance == null)
                InjectionContainer.Instance = ContainerBuilderProvider.Builder.Build();

            Session = InjectionContainer.Instance.Resolve<ISession>();

            UniqueValueGenerator.Init();
        }

        protected ISession OpenSession()
        {
            return InjectionContainer.Instance.Resolve<ISession>();
        }

        [TearDown]
        public void TestTearDown()
        {
          
        }
    }
}