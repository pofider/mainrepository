﻿using Autofac;
using MFR.Infrastructure.Builders;
using NHibernate;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;
using Postuj.Infrastructure.DependencyInjection;

namespace Postuj.Test
{
    public class where_test_access_database
    {
        protected ISession Session { get; private set; }
        protected bool UserTransaction = true;
        protected UserProfile CurrentUser { get; private set; }

        static where_test_access_database()
        {
            new OrmConfigure()
                    .ForAssemblyType<Post>()
                    .CreateSchema()
                    .WithSessionPerDependency()
                    .ConfiguraOrm();

            InjectionContainer.Instance = ContainerBuilderProvider.Builder.Build();
        }

        [SetUp]
        public void TestSetUp()
        {
            Session = InjectionContainer.Instance.Resolve<ISession>();
            Session.Transaction.Begin();

            CurrentUser = new UserProfileBuilder(true).BuildAndWriteToDatabase(Session);

            UniqueValueGenerator.Init();
        }

        [TearDown]
        public void TestTearDown()
        {
            Session.Transaction.Rollback();
        }
    }
}