﻿using NUnit.Framework;
using Postuj.Infrastructure.Command;
using Rhino.Mocks;

namespace Postuj.Test
{
    public class where_test_tests_command<TCommand> : where_test_access_database where TCommand : ICommand, new()
    {
        protected TCommand SUT;
        protected ICommandExecutor CommandExecutor;
        

        [SetUp]
        public void CommandTestSetUp()
        {
            CommandExecutor = MockRepository.GeneratePartialMock<CommandExecutor>(Session, null, null);
            
            SUT = new TCommand();
            SUT.CommandExecutor = CommandExecutor;
            SUT.Cache = new NullCache();
            SUT.Session = Session;
            SUT.CurrentUser = CurrentUser;
        }

        [TearDown]
        public void CommandTestTearDown()
        {
        } 
    }
}