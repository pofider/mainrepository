﻿using NUnit.Framework;
using Postuj.Infrastructure.Query;
using Rhino.Mocks;

namespace Postuj.Test
{
    public class where_test_tests_query<TQuery, TResult> : where_test_access_database where TQuery : IQuery<TResult>, new() 
    {
        protected TQuery SUT;
        protected IQueryExecutor QueryExecutor;

        [SetUp]
        public void QueryTestSetUp()
        {
            QueryExecutor = MockRepository.GeneratePartialMock<QueryExecutor>(Session, null, new NullCache());

            SUT = new TQuery();
            SUT.Session = Session;
            SUT.CurrentUser = CurrentUser;
            SUT.QueryExecutor = QueryExecutor;
        }
    }
}