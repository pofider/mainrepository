﻿using Postuj.Infrastructure.DependencyInjection;

namespace Postuj.Test
{
    public class where_unit_testing
    {
        static where_unit_testing()
        {
            InjectionContainer.Instance = ContainerBuilderProvider.Builder.Build();
        }
    }
}