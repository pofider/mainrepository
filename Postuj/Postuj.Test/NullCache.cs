﻿using System;
using Postuj.Infrastructure.Cache;

namespace Postuj.Test
{
    public class NullCache : ICache 
    {
        public object this[string key]
        {
            get { return null; }
            set { ; }
        }
        

        public object GetOrAdd(string key, Func<string, object> func)
        {
            return func(key);
        }

        public void Clear()
        {
            
        }
    }
}