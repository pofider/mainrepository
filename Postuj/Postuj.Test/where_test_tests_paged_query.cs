﻿using System.Linq;
using NUnit.Framework;
using Postuj.Infrastructure.DataAccess.ORM;
using Postuj.Infrastructure.Query;
using Rhino.Mocks;

namespace Postuj.Test
{
    [TestFixture]
    public abstract class where_test_tests_paged_query<TQuery, TEntity, TResult> : where_test_tests_query<TQuery, TResult> where TQuery : IQuery<TResult>, new()
        where TEntity : EntityBase
    {
        [Test]
        public void should_return_one_item_by_default()
        {
            CreateAndSaveInputData();

            var result = SUT.Execute();

            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void should_return_proper_page()
        {
            CreateAndSaveInputData();
            CreateAndSaveInputData();

            Session.Flush();

            SUT.PageNumber = 2;
            SUT.PageSize = 1;
            var result = SUT.Execute();

            Assert.AreEqual(1, result.Count());
        }

        protected abstract TEntity CreateAndSaveInputData();
    }
}