﻿using System.Linq;
using NHibernate.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class CreateOrFindUserCommandTest : where_test_tests_command<CreateOrFindUserCommand>
    {
        [Test]
        public void should_create_user()
        {
            SUT.Username = "Karel";
            SUT.Provider = "facebook";
            SUT.ProviderUserId = "123";

            SUT.Execute();

            Assert.AreEqual(SUT.Result.Result.Username, SUT.Username);
            Assert.AreEqual(SUT.Result.Result.Provider, SUT.Provider);
            Assert.AreEqual(SUT.Result.Result.ProviderUserId, SUT.ProviderUserId);

            Assert.IsTrue(Session.Query<UserProfile>().Any(u => u.Username == SUT.Username));
        }

        [Test]
        public void should_find_user()
        {
            var user = new UserProfileBuilder(true).WithProviderUserId("324").BuildAndWriteToDatabase(Session);
            SUT.Username = user.Username;
            SUT.Provider = user.Provider;
            SUT.ProviderUserId = user.ProviderUserId;

            SUT.Execute();

            Assert.AreEqual(SUT.Result.Result.Username, SUT.Username);
        }
    }
}