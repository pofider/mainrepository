﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class AnswerCommandTest : where_test_tests_command<AnswerCommand>
    {
        [Test]
        public void should_add_answer()
        {
            var post = new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;

            SUT.Execute();

            Assert.IsTrue(SUT.Result.Result);
            Assert.AreEqual(1, post.Answers.Count);
        }

        [Test]
        public void should_not_add_answer_when_user_already_answered()
        {
            var post = new PostBuilder().WithAnswer(new AnswerBuilder(true)
                                                        .WithAuthor(CurrentUser).Build())
                                                        .BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;

            SUT.Execute();

            Assert.IsFalse(SUT.Result.Result);
            Assert.AreEqual(1, post.Answers.Count);
        }
    }
}