﻿using System;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class VoteCommandTest : where_test_tests_command<VoteCommand>
    {
        [Test]
        public void should_add_vote()
        {
            var post = new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;

            SUT.Execute();

            Assert.IsTrue(SUT.Result.Result);
            Assert.AreEqual(1, post.Votes.Count);
        }

        [Test]
        public void should_not_add_vote_when_user_already_voted()
        {
            var post = new PostBuilder().WithVote(new VoteBuilder(true)
                                                        .WithAuthor(CurrentUser).Build())
                                                        .BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;

            SUT.Execute();

            Assert.IsFalse(SUT.Result.Result);
            Assert.AreEqual(1, post.Votes.Count);
        }

        [Test]
        public void should_add_voting_day_score()
        {
            var post = new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;

            SUT.Execute();
            
            Assert.AreEqual(1 + (Post.EXPECTED_MAX_SCORE * (long)TimeSpan.FromTicks(DateTime.Now.Ticks).TotalDays), post.VotesDayScore);
        }
    }
}