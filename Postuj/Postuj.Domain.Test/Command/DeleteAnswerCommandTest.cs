﻿using System.Linq;
using NHibernate.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class DeleteAnswerCommandTest : where_test_tests_command<DeleteAnswerCommand>
    {
        [Test]
        public void should_remove_answer_from_post()
        {
            var post =
                new PostBuilder().WithAnswer(new AnswerBuilder().WithAuthor(CurrentUser).Build())
                                 .BuildAndWriteToDatabase(Session);

            SUT.PostId = post.Id.Value;
            SUT.Execute();
            
            post = Session.Query<Post>().Single(p => p.Id == post.Id);

            Assert.AreEqual(0, post.Answers.Count);
        }
    }
}