﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class CreateCommentCommandTest : where_test_tests_command<CreateCommentCommand>
    {
         [Test]
         public void should_add_comment_to_post()
         {
             var post = new PostBuilder().BuildAndWriteToDatabase(Session);

             SUT.CommentDto = new CommentDto() {PostId = post.Id.Value, Content = "content"};
             SUT.Execute();

             post = Session.Load<Post>(post.Id);
             Assert.AreEqual(post.Comments.First().Author, CurrentUser);
             Assert.AreEqual(1, post.Comments.Count);
         }

         [Test]
         public void should_add_commeny_day_score()
         {
             var post = new PostBuilder().BuildAndWriteToDatabase(Session);

             SUT.CommentDto = new CommentDto() { PostId = post.Id.Value, Content = "content" };
             SUT.Execute();

             post = Session.Load<Post>(post.Id);

             Assert.AreEqual(1 + (Post.EXPECTED_MAX_SCORE * (long)TimeSpan.FromTicks(DateTime.Now.Ticks).TotalDays), post.CommentsDayScore);
         }
    }
}