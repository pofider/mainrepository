﻿using System.Linq;
using BitlyDotNET.Implementations;
using BitlyDotNET.Interfaces;
using NHibernate.Linq;
using NUnit.Framework;
using Postuj.Domain.Command;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Test;
using Rhino.Mocks;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class CreatePostCommandTest : where_test_tests_command<CreatePostCommand>
    {
        [SetUp]
        public void SetUp()
        {
            SUT.BitlyService = MockRepository.GenerateMock<IBitlyService>();
        }

        [Test]
        public void should_create_post()
        {
            SUT.PostDto = new PostDto() { Content = "testContent", CategoryCode = "news"};

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.AreEqual(SUT.PostDto.Content, post.Content);
            Assert.AreEqual(CurrentUser, post.Author);
            Assert.AreEqual(SUT.PostDto.CategoryCode.ToCategoryEnum(), post.Category);
        }

        [Test]
        public void should_replace_line_breaks()
        {
            SUT.PostDto = new PostDto() { Content = "testCo\nntent", CategoryCode = "news" };

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.IsTrue(post.Content.Contains("<br>"));
        }

        [Test]
        public void should_replace_links()
        {
            SUT.PostDto = new PostDto() { Content = "http://idnes.cz", CategoryCode = "news" };

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.AreEqual("<a href='http://idnes.cz' target='blank'>http://idnes.cz</a>", post.Content);
        }

        [Test]
        public void should_shorten_links()
        {
            SUT.PostDto = new PostDto() { Content = "http://idnes.cz", CategoryCode = "news" };

            SUT.BitlyService = new BitlyService("pofider", "R_107726339a3b6bc1c3a073a5cddfa825");
            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.IsTrue(post.Content.Contains("bit"));
        }

        [Test]
        public void should_replace_links_without_http()
        {
            SUT.PostDto = new PostDto() { Content = "www.idnes.cz", CategoryCode = "news" };

            SUT.Execute();
            
            var post = Session.Query<Post>().Single();
            Assert.AreEqual("<a href='http://www.idnes.cz' target='blank'>www.idnes.cz</a>", post.Content);
        }

        [Test]
        public void should_replace_links_with_https()
        {
            SUT.PostDto = new PostDto() { Content = "https://idnes.cz", CategoryCode = "news" };

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.AreEqual("<a href='https://idnes.cz' target='blank'>https://idnes.cz</a>", post.Content);
        }

        [Test]
        public void should_work_with_additional_youtube_format()
        {
            SUT.PostDto = new PostDto() { Content = "http://www.youtube.com/watch?v=-wtIMTCHWuI&feature=youtu.be rest of message", CategoryCode = "news" };

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.AreEqual("<img src='http://img.youtube.com/vi/-wtIMTCHWuI/0.jpg'  height='220' width='329'</img>", post.VideoThumb);
            Assert.AreEqual("rest of message", post.Content);
        }

        [Test]
        public void should_work_with_youtube_format()
        {
            SUT.PostDto = new PostDto() { Content = "http://www.youtube.com/watch?v=A3PDXmYoF5U", CategoryCode = "news" };

            SUT.Execute();

            var post = Session.Query<Post>().Single();
            Assert.AreEqual("<img src='http://img.youtube.com/vi/A3PDXmYoF5U/0.jpg'  height='220' width='329'</img>", post.VideoThumb);
            Assert.AreEqual(string.Empty, post.Content);
        }
    }
}