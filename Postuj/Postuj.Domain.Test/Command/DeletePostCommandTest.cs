﻿using System.Linq;
using NHibernate.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class DeletePostCommandTest : where_test_tests_command<DeletePostCommand>
    {
        [Test]
        public void should_fail_when_post_does_not_belong_to_current_user()
        {
            var post = new PostBuilder(true).BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;
            SUT.Execute();

            Assert.IsFalse(SUT.Result.IsSuccess);
        }

        [Test]
        public void should_pass_when_current_user_is_admin()
        {
            var post = new PostBuilder(true).BuildAndWriteToDatabase(Session);
            CurrentUser.IsAdmin = true;

            SUT.Id = post.Id.Value;
            SUT.Execute();

            Assert.IsTrue(SUT.Result.IsSuccess);
        }

        [Test]
        public void should_delete_post()
        {
            var post = new PostBuilder(true).WithAuthor(CurrentUser).BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;
            SUT.Execute();

            Assert.IsFalse(Session.Query<Post>().Any(p => p.Id == post.Id));
        }

        [Test]
        public void should_delete_relaed_comments()
        {
            var post = new PostBuilder(true)
                            .WithAuthor(CurrentUser)
                            .WithComment(new CommentBuilder(true).Build())
                            .BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;
            SUT.Execute();

            Assert.IsFalse(Session.Query<Comment>().Any());
        }

        [Test]
        public void should_delete_related_votes()
        {
            var post = new PostBuilder(true)
                            .WithAuthor(CurrentUser)
                            .WithVote(new VoteBuilder(true).Build())
                            .BuildAndWriteToDatabase(Session);

            SUT.Id = post.Id.Value;
            SUT.Execute();

            Assert.IsFalse(Session.Query<Vote>().Any());
        }
    }
}