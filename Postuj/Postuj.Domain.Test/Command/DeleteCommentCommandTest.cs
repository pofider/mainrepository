﻿using System.Linq;
using NHibernate.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Command;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Command
{
    [TestFixture]
    public class DeleteCommentCommandTest : where_test_tests_command<DeleteCommentCommand>
    {
        [Test]
        public void should_delete_comment()
        {
            var post =
                new PostBuilder().WithComment(new CommentBuilder().WithAuthor(CurrentUser).Build())
                                 .BuildAndWriteToDatabase(Session);

            SUT.CommentId = post.Comments.Single().Id.Value;
            SUT.Execute();

            post = Session.Query<Post>().Where(p => p.Id == post.Id).Single();

            Assert.AreEqual(0, post.Comments.Count);
        }

        [Test]
        public void should_fail_when_commend_not_owned_by_current_user()
        {
            var post =
                new PostBuilder().WithComment(new CommentBuilder().Build())
                                 .BuildAndWriteToDatabase(Session);

            SUT.CommentId = post.Comments.Single().Id.Value;
            SUT.Execute();

            Assert.IsFalse(SUT.Result.Result);
        }
    }
}