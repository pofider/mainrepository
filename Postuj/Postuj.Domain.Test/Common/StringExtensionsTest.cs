﻿using NUnit.Framework;
using Postuj.Infrastructure.Common;

namespace Postuj.Domain.Test.Common
{
    [TestFixture]
    public class StringExtensionsTest
    {
        [Test]
        public void FormatUrls_for_single()
        {
            Assert.AreEqual("<a href='http://a.cz' target='blank'>http://a.cz</a>", "http://a.cz".FormatUrls());
        }

        [Test]
        public void FormatUrls_for_double()
        {
            var content = "http://a.cz http://a.cz";
            var result = content.FormatUrls();

            Assert.AreEqual("<a href='http://a.cz' target='blank'>http://a.cz</a> <a href='http://a.cz' target='blank'>http://a.cz</a>", result);
        }

        [Test]
        public void FormatUrls_advandec()
        {
            var content = "http://a.cz kuk www.kuk.cz kuk2";
            var result = content.FormatUrls();

            Assert.AreEqual("<a href='http://a.cz' target='blank'>http://a.cz</a> kuk <a href='http://www.kuk.cz' target='blank'>www.kuk.cz</a> kuk2", result);
        }
    }
}