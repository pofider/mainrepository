﻿using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Domain.Shared;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class CommentListQueryTest : where_test_tests_paged_query<CommentListQuery, Comment, CommentListDto>
    {
        [SetUp]
        public void SetUp()
        {
            _post = null;
        }

        [Test]
        public void should_return_comment_wit_filled_properties()
        {
            var comment = new CommentBuilder().BuildAndWriteToDatabase(Session);

            SUT.PostId = comment.Post.Id.Value;
            dynamic result = SUT.Execute().Single();

            Assert.AreEqual(comment.Content, result.Content);
            Assert.AreEqual(comment.Id, result.Id);
            Assert.AreEqual(comment.CreationDate.ToString("dd.MM.yyyy HH:mm"), result.CreationDateString);
            Assert.AreEqual(comment.Author.Username, result.Username);
            Assert.AreEqual(comment.Author.ProfileImage.Url, result.ProfileImageLink);
            Assert.AreEqual(comment.Author.ProfileLink.Url, result.ProfileLink);
        }

        [Test]
        public void should_filter_by_post()
        {
            var comment = new CommentBuilder().BuildAndWriteToDatabase(Session);

            SUT.PostId = comment.Post.Id.Value + 1;//different post id

            Assert.AreEqual(0, SUT.Execute().Count());

        }

        [Test]
        public void should_filter_by_commentId()
        {
            var comment = new CommentBuilder().BuildAndWriteToDatabase(Session);

            SUT.CommentId = comment.Id.Value + 1;//different comment id

            Assert.AreEqual(0, SUT.Execute().Count());

        }

        private Post _post;
        protected override Comment CreateAndSaveInputData()
        {
            if (_post == null)
                _post = new PostBuilder().BuildAndWriteToDatabase(Session);

            var comment = new CommentBuilder().WithPost(_post).BuildAndWriteToDatabase(Session);
            SUT.PostId = comment.Post.Id.Value;
            return comment;
        }
    }
}