﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class AnswerListQueryTest : where_test_tests_query<AnswerListQuery, object>
    {
        [Test]
        public void should_fill_properties()
        {
            var post =
                new PostBuilder().WithAnswer(new AnswerBuilder().Build())
                                 .WithAuthor(CurrentUser)
                                 .BuildAndWriteToDatabase(Session);
            var answer = post.Answers.Single();

            SUT.PostId = post.Id.Value;
            dynamic result = SUT.Execute().Single();

            Assert.AreEqual(answer.Author.Username, result.Username);
            Assert.AreEqual(answer.Author.ProfileImage.Url, result.ProfileImageLink);
            Assert.AreEqual(answer.Id, result.Id);
        }

        [Test]
        public void should_filter_by_post()
        {
            var post =
                new PostBuilder().WithAnswer(new AnswerBuilder().Build())
                                 .WithAuthor(CurrentUser)
                                 .BuildAndWriteToDatabase(Session);

            var post2 =
               new PostBuilder().WithAnswer(new AnswerBuilder().Build())
                                .WithAuthor(CurrentUser)
                                .BuildAndWriteToDatabase(Session);
          
            SUT.PostId = post.Id.Value;
            Assert.AreEqual(1, SUT.Execute().Count());
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void should_throw_when_post_not_owned_by_current_user()
        {
            var post =
                new PostBuilder().WithAnswer(new AnswerBuilder().Build())
                                 .BuildAndWriteToDatabase(Session);

            SUT.PostId = post.Id.Value;
            SUT.Execute().Any();
        }
    }
}