﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    public class OldNotificationCompositeQueryTest : where_test_tests_query<OldNotificationCompositeQuery, object>
    {
        [Test]
        public void should_union_results()
        {
            new VoteBuilder(true)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            new CommentBuilder(true)
                        .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                        .WithAuthor(new UserProfileBuilder().Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            new AnswerBuilder(true)
                    .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                    .WithAuthor(new UserProfileBuilder().Build())
                    .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                    .BuildAndWriteToDatabase(Session);

            Assert.AreEqual(3, SUT.Execute().Count());
        }

        [Test]
        public void should_load_just_old_notifications()
        {
            new VoteBuilder(true)
                         .WithAuthor(CurrentUser)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(1))
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            new CommentBuilder(true)
                        .WithAuthor(CurrentUser)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            Assert.AreEqual(0, SUT.Execute().Count());
        }

        [Test]
        public void should_order_by_creationDate()
        {
            new VoteBuilder(true)
                         .WithCreationDate(new DateTime(2001, 01, 01))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            var newestComment = new CommentBuilder(true)
                        .WithCreationDate(new DateTime(2003, 01, 01))
                        .WithAuthor(new UserProfileBuilder().Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            new VoteBuilder(true)
                         .WithCreationDate(new DateTime(2001, 01, 01))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            dynamic notification = SUT.Execute().First();
            Assert.AreEqual(newestComment.Id, notification.Id);
        }

        [Test]
        public void should_apply_paging()
        {
            SUT.PageSize = 1;
            SUT.PageNumber = 1;

            new VoteBuilder(true)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            new CommentBuilder(true)
                        .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                        .WithAuthor(new UserProfileBuilder().Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            Assert.AreEqual(1, SUT.Execute().Count());
        }
    }
}