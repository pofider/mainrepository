﻿using System;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class UserCardQueryTest : where_test_tests_query<UserCardQuery, object>
    {
        [Test]
        public void should_return_proper_count()
        {
            CurrentUser.LastNotificationDisplayDate = DateTime.Now.AddYears(-30);

            new PostBuilder()
                .WithVote(new VoteBuilder().Build())
                .WithAnswer(new AnswerBuilder().WithAuthor(CurrentUser).Build())
                .WithComment(new CommentBuilder().Build())
                .WithAuthor(CurrentUser)
                .BuildAndWriteToDatabase(Session);

            dynamic result = SUT.ExecuteSingle();

            Assert.AreEqual(1, result.PostCount);
            Assert.AreEqual(1, result.AnswerCount);
            Assert.AreEqual(2, result.NotificationCount);
        }
    }
}