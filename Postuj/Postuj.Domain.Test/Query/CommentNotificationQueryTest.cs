﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class CommentNotificationQueryTest : where_test_tests_paged_query<CommentNotificationQuery, Comment, object>
    {
        [Test]
        public void should_set_result_properties()
        {
            var comment = CreateAndSaveInputData();

            dynamic result = SUT.Execute().Single();

            Assert.That(result.CreationDate, Is.EqualTo(comment.CreationDate).Within(1000).Milliseconds);
            Assert.AreEqual(comment.Post.Content, result.PostContent);
            Assert.AreEqual(comment.Content, result.Content);
            Assert.AreEqual(comment.Author.Username, result.Username);
            Assert.AreEqual(comment.Post.Id, result.PostId);
            Assert.AreEqual(CommentNotificationQuery.COMMENT_NOTIFICATION, result.Type);
            Assert.AreEqual(comment.Author.ProfileLink.Url, result.ProfileLink);
        }

        [Test]
        public void should_order_notificatoins_by_time_desc()
        {
            var comment1 = new CommentBuilder(true)
                        .WithCreationDate(new DateTime(2012, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var comment2 = new CommentBuilder(true)
                        .WithCreationDate(new DateTime(2013, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var comment3 = new CommentBuilder(true)
                        .WithCreationDate(new DateTime(2011, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            dynamic result = SUT.Execute().First();

            Assert.AreEqual(comment2.Id, result.Id);
        }

        [Test]
        public void should_filter_by_currentUser()
        {
            var comment = new CommentBuilder(true).BuildAndWriteToDatabase(Session);

            Assert.IsFalse(SUT.Execute().Any());
        }

        protected override Comment CreateAndSaveInputData()
        {
            return new CommentBuilder(true)
                        .WithAuthor(new UserProfileBuilder(true).Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
        }
    }
}