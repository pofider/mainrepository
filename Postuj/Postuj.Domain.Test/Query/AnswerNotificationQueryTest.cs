﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class AnswerNotificationQueryTest : where_test_tests_paged_query<AnswerNotificationQuery, Answer, object>
    {
        [Test]
        public void should_set_result_properties()
        {
            var answer = CreateAndSaveInputData();

            dynamic result = SUT.Execute().Single();

            Assert.That(result.CreationDate, Is.EqualTo(answer.CreationDate).Within(1000).Milliseconds);
            Assert.AreEqual(answer.Post.Content, result.Content);
            Assert.AreEqual(answer.Author.Username, result.Username);
            Assert.AreEqual(answer.Post.Id, result.PostId);
            Assert.AreEqual(AnswerNotificationQuery.ANSWER_MEETING_NOTIFICATION, result.Type);
            Assert.AreEqual(answer.Author.ProfileLink.Url, result.ProfileLink);
        }

        [Test]
        public void should_order_notificatoins_by_time_desc()
        {
            var a1 = new AnswerBuilder(true)
                        .WithCreationDate(new DateTime(2012, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var a2 = new AnswerBuilder(true)
                        .WithCreationDate(new DateTime(2013, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var a3 = new AnswerBuilder(true)
                        .WithCreationDate(new DateTime(2011, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            dynamic result = SUT.Execute().First();

            Assert.AreEqual(a2.Id, result.Id);
        }

        [Test]
        public void should_filter_by_currentUser()
        {
            var a = new AnswerBuilder(true).BuildAndWriteToDatabase(Session);

            Assert.IsFalse(SUT.Execute().Any());
        }

        protected override Answer CreateAndSaveInputData()
        {
            return new AnswerBuilder(true)
                        .WithAuthor(new UserProfileBuilder(true).Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
        }
    }
}