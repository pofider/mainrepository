﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class NewNotificationCompositeQueryTest : where_test_tests_query<NewNotificationCompositeQuery, object>
    {
        [Test]
        public void should_union_results()
        {
            new VoteBuilder(true)
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            new CommentBuilder(true)
                        .WithAuthor(new UserProfileBuilder().Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            new AnswerBuilder(true)
                      .WithAuthor(new UserProfileBuilder().Build())
                      .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                      .BuildAndWriteToDatabase(Session);

            Assert.AreEqual(3, SUT.Execute().Count());
        }

        [Test]
        public void should_load_just_new_notifications()
        {
            new VoteBuilder(true)
                         .WithAuthor(CurrentUser)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            new CommentBuilder(true)
                        .WithAuthor(CurrentUser)
                         .WithCreationDate(CurrentUser.LastNotificationDisplayDate.AddDays(-1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            Assert.AreEqual(0, SUT.Execute().Count());
        }

        [Test]
        public void should_order_by_creationDate()
        {
            new VoteBuilder(true)
                         .WithCreationDate(new DateTime(2016, 01, 01))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            var newestComment = new CommentBuilder(true)
                        .WithCreationDate(new DateTime(2017, 01, 01))
                        .WithAuthor(new UserProfileBuilder().Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            new VoteBuilder(true)
                         .WithCreationDate(new DateTime(2015, 01, 01))
                         .WithAuthor(new UserProfileBuilder().Build())
                         .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                         .BuildAndWriteToDatabase(Session);

            dynamic notification = SUT.Execute().First();
            Assert.AreEqual(newestComment.Id, notification.Id);
        }
    }
}