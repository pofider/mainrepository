﻿using System;
using System.Linq;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Test;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class VoteNotificationQueryTest : where_test_tests_paged_query<VoteNotificationQuery, Vote, object>
    {
        [Test]
        public void should_set_result_properties()
        {
            var vote = CreateAndSaveInputData();

            dynamic result = SUT.Execute().Single();

            Assert.That(result.CreationDate, Is.EqualTo(vote.CreationDate).Within(1000).Milliseconds);
            Assert.AreEqual(vote.Post.Content, result.Content);
            Assert.AreEqual(vote.IsLike, result.IsLike);
            Assert.AreEqual(vote.Author.Username, result.Username);
            Assert.AreEqual(vote.Post.Id, result.PostId);
            Assert.AreEqual(VoteNotificationQuery.VOTE_NOTIFICATION, result.Type);
            Assert.AreEqual(vote.Author.ProfileLink.Url, result.ProfileLink);
        }

        [Test]
        public void should_order_notificatoins_by_time_desc()
        {
            var vote1 = new VoteBuilder(true)
                        .WithCreationDate(new DateTime(2012,1,1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var vote2 = new VoteBuilder(true)
                        .WithCreationDate(new DateTime(2013, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
            var vote3 = new VoteBuilder(true)
                        .WithCreationDate(new DateTime(2011, 1, 1))
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);

            dynamic result = SUT.Execute().First();

            Assert.AreEqual(vote2.Id, result.Id);
        }

        [Test]
        public void should_filter_by_currentUser()
        {
            var vote = new VoteBuilder(true).BuildAndWriteToDatabase(Session);

            Assert.IsFalse(SUT.Execute().Any());
        }

        protected override Vote CreateAndSaveInputData()
        {
            return new VoteBuilder(true)
                        .WithAuthor(new UserProfileBuilder(true).Build())
                        .WithPost(new PostBuilder(true).WithAuthor(CurrentUser).Build())
                        .BuildAndWriteToDatabase(Session);
        }
    }
}