﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Domain.Shared;
using Postuj.Test;
using System.Linq;

namespace Postuj.Domain.Test.Query
{
    [TestFixture]
    public class PostListQueryTest : where_test_tests_paged_query<PostListQuery, Post, PostListDto>
    {
        [Test]
        public void should_return_post_with_filled_properties()
        {
            var post = new PostBuilder()
                                .WithVote(new VoteBuilder().WithIsLike(true).Build())
                                .WithVote(new VoteBuilder().WithIsLike(false).Build())
                                .WithVote(new VoteBuilder().WithIsLike(false).Build())
                                .WithAnswer(new AnswerBuilder().Build())
                                .WithComment(new CommentBuilder().Build())
                                .BuildAndWriteToDatabase(Session);

            dynamic result = SUT.Execute().Single();

            Assert.AreEqual(post.Content, result.Content);
            Assert.AreEqual(1, result.Likes);
            Assert.AreEqual(2, result.Dislikes);
            Assert.AreEqual(1, result.Comments);
            Assert.AreEqual(post.Location, result.Location);
            Assert.AreEqual(post.Category.ToCategoryEnumCode(), result.CategoryCode);
            Assert.AreEqual(post.CreationDate, result.CreationDate);
            Assert.AreEqual(post.Id, result.Id);
            Assert.AreEqual(post.Location.ToEnumString(), result.LocationString);
            Assert.AreEqual(post.Category.ToEnumString(), result.CategoryString);
            Assert.AreEqual(post.CreationDate.ToTimeFromNowString(), result.CreationDateString);
            Assert.AreEqual(post.Author.Username, result.Username);
            Assert.AreEqual(post.Author.ProfileImage.Url, result.ProfileImageLink);
            Assert.AreEqual(post.ImageReference.PublicUri, result.AttachedImageLink);
            Assert.AreEqual(post.ThumbImageReference.PublicUri, result.AttachedImageThumbLink);
            Assert.AreEqual(1, result.Answers);
            Assert.AreEqual(post.VideoLink, result.VideoLink);
            Assert.AreEqual(post.Author.ProfileLink.Url, result.ProfileLink);

            //dynamic commentList = result.CommentsList;
            //dynamic comment = commentList.First();
            //Assert.AreEqual(post.Comments.First().Content, comment.Content);
        }

        [Test]
        public void should_filter_by_location()
        {
            new PostBuilder().WithLocation(LocationEnum.Praha).BuildAndWriteToDatabase(Session);
            
            SUT.Location = LocationEnum.Zlinsky;

            Assert.AreEqual(0, SUT.Execute().Count());
        }


        [Test]
        public void should_filter_by_id()
        {
            var post = new PostBuilder().BuildAndWriteToDatabase(Session);
            new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.PostId = post.Id;

            Assert.AreEqual(1, SUT.Execute().Count());
        }

        [Test]
        public void should_filter_by_category()
        {
            new PostBuilder().WithCategory(CategoryEnum.News).BuildAndWriteToDatabase(Session);

            SUT.Category = CategoryEnum.Media;

            Assert.AreEqual(0, SUT.Execute().Count());
        }

        [Test]
        public void should_filter_by_posted_by()
        {
            new PostBuilder().WithCategory(CategoryEnum.News).BuildAndWriteToDatabase(Session);

            SUT.PostedBy = new UserProfileBuilder().BuildAndWriteToDatabase(Session);

            Assert.AreEqual(0, SUT.Execute().Count());
        }

        [Test]
        public void should_filter_by_posted_by_me()
        {
            new PostBuilder().WithCategory(CategoryEnum.News).BuildAndWriteToDatabase(Session);

            SUT.PostedByMe = true;

            Assert.AreEqual(0, SUT.Execute().Count());
        }

        [Test]
        public void should_filter_by_search_string()
        {
            new PostBuilder().WithContent("xxxx šéárch yyyyy").BuildAndWriteToDatabase(Session);
            new PostBuilder().WithAuthor(new UserProfileBuilder().WithUsername("šearch").Build()).WithContent("xxxx non yyyyy").BuildAndWriteToDatabase(Session);
            new PostBuilder().WithContent("xxxx non yyyyy").BuildAndWriteToDatabase(Session);

            SUT.SearchString = "search";

            Assert.AreEqual(2, SUT.Execute().Count());
        }



        [Test]
        public void should_sort_by_creationDate()
        {
            var post1 = new PostBuilder().WithCreationDate(new DateTime(2012, 01, 01)).BuildAndWriteToDatabase(Session);
            var post2 = new PostBuilder().WithCreationDate(new DateTime(2013, 01, 01)).BuildAndWriteToDatabase(Session);
            var post3 = new PostBuilder().WithCreationDate(new DateTime(2011, 01, 01)).BuildAndWriteToDatabase(Session);

            dynamic first = SUT.Execute().First();

            Assert.AreEqual(post2.Id, first.Id);
        }

        [Test]
        public void should_sort_by_likes_when_specified()
        {
            var post1 = new PostBuilder().WithVotesDayScore(10).BuildAndWriteToDatabase(Session);
            var post2 = new PostBuilder().WithVotesDayScore(30).BuildAndWriteToDatabase(Session);
            var post3 = new PostBuilder().WithVotesDayScore(20).BuildAndWriteToDatabase(Session);

            SUT.PostOrder = PostOrderEnum.Likes;

            dynamic first = SUT.Execute().First();

            Assert.AreEqual(post2.Id, first.Id);
        }

        [Test]
        public void should_sort_by_comments_when_specified()
        {
            var post1 = new PostBuilder().WithCommentsDayScore(10).BuildAndWriteToDatabase(Session);
            var post2 = new PostBuilder().WithCommentsDayScore(30).BuildAndWriteToDatabase(Session);
            var post3 = new PostBuilder().WithCommentsDayScore(20).BuildAndWriteToDatabase(Session);

            SUT.PostOrder = PostOrderEnum.Comments;

            dynamic first = SUT.Execute().First();
 
            Assert.AreEqual(post2.Id, first.Id);
        }

        [Test]
        public void should_sort_by_likes_all_when_specified()
        {
            var post1 = new PostBuilder().WithVote(new VoteBuilder(true).Build()).WithVotesDayScore(100).BuildAndWriteToDatabase(Session);
            var post2 = new PostBuilder().WithVote(new VoteBuilder(true).Build()).WithVote(new VoteBuilder(true).Build()).WithVotesDayScore(50).BuildAndWriteToDatabase(Session);
            var post3 = new PostBuilder().WithVotesDayScore(10).BuildAndWriteToDatabase(Session);
            
            SUT.PostOrder = PostOrderEnum.LikesAll;

            dynamic first = SUT.Execute().First();

            Assert.AreEqual(post2.Id, first.Id);
        }

        [Test]
        public void should_sort_by_comments_all_when_specified()
        {
            var post1 = new PostBuilder().WithComment(new CommentBuilder(true).Build()).WithCommentsDayScore(100).BuildAndWriteToDatabase(Session);
            var post2 = new PostBuilder().WithComment(new CommentBuilder(true).Build()).WithComment(new CommentBuilder(true).Build()).WithCommentsDayScore(50).BuildAndWriteToDatabase(Session);
            var post3 = new PostBuilder().WithCommentsDayScore(10).BuildAndWriteToDatabase(Session);

            SUT.PostOrder = PostOrderEnum.CommentsAll;

            dynamic first = SUT.Execute().First();

            Assert.AreEqual(post2.Id, first.Id);
        }



        [Test]
        public void should_filter_by_commentedBy_when_specified()
        {
            var user = new UserProfileBuilder().BuildAndWriteToDatabase(Session);

            new PostBuilder().WithComment(new CommentBuilder().WithAuthor(user).Build()).BuildAndWriteToDatabase(Session);
            new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.CommentedBy = user;

            var resut = SUT.Execute();

            Assert.AreEqual(1, resut.Count());
        }

        [Test]
        public void should_filter_by_answeredBy_when_specified()
        {
            new PostBuilder().WithAnswer(new AnswerBuilder().WithAuthor(CurrentUser).Build()).BuildAndWriteToDatabase(Session);
            new PostBuilder().BuildAndWriteToDatabase(Session);

            SUT.AnsweredBy = CurrentUser;

            var resut = SUT.Execute();

            Assert.AreEqual(1, resut.Count());
        }

        [Test]
        public void should_fill_has_my_answers_if_true()
        {
            new PostBuilder().WithAnswer(new AnswerBuilder().WithAuthor(CurrentUser).Build()).BuildAndWriteToDatabase(Session);
            
            dynamic resut = SUT.Execute().Single();

            Assert.IsTrue(resut.HasMyAnswer);
        }

        [Test]
        public void should_fill_has_my_answers_if_false()
        {
            new PostBuilder().WithAnswer(new AnswerBuilder().WithAuthor(new UserProfileBuilder(true).Build()).Build()).BuildAndWriteToDatabase(Session);

            dynamic resut = SUT.Execute().Single();

            Assert.IsFalse(resut.HasMyAnswer);
        }

        [Test]
        public void should_update_date_info_in_invariant()
        {
            new PostBuilder().BuildAndWriteToDatabase(Session);

            IEnumerable<PostListDto> result = SUT.Execute();

            IEnumerable<object> afterInvariant = SUT.Invariant(result);
        }

        protected override Post CreateAndSaveInputData()
        {
            return new PostBuilder().BuildAndWriteToDatabase(Session);
        }
    }
}