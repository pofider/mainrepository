﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Persistence
{
    [TestFixture]
    public class UserProfilePersistenceTest : where_test_tests_entity_persistance<UserProfile>
    {
        [Test]
        public void TestPersistence()
        {
            SaveAndLoad();

            Assert.AreEqual(SavedEntity.Provider, LoadEntity.Provider);
            Assert.AreEqual(SavedEntity.ProviderUserId, LoadEntity.ProviderUserId);
            Assert.AreEqual(SavedEntity.Username, LoadEntity.Username);
            Assert.AreEqual(SavedEntity.IsAdmin, LoadEntity.IsAdmin);
            Assert.That(LoadEntity.LastNotificationDisplayDate, Is.EqualTo(SavedEntity.LastNotificationDisplayDate).Within(1000).Milliseconds);
        }

        protected override UserProfile CreateAndSaveEntity()
        {
            return new UserProfileBuilder(true).BuildAndWriteToDatabase(Session);
        }
    }
}