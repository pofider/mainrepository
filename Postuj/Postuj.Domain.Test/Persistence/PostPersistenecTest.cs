﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Persistence
{
    [TestFixture]
    public class PostPersistenecTest : where_test_tests_entity_persistance<Post>
    {
        [Test]
        public void TestPersistence()
        {
            SaveAndLoad();
            
            Assert.AreEqual(SavedEntity.Content, LoadEntity.Content);
            Assert.AreEqual(SavedEntity.Author, LoadEntity.Author);
            Assert.AreEqual(SavedEntity.Comments.Count, LoadEntity.Comments.Count);
            Assert.AreEqual(SavedEntity.Category, LoadEntity.Category);
            Assert.AreEqual(SavedEntity.Location, LoadEntity.Location);
            Assert.AreEqual(SavedEntity.CreationDate, LoadEntity.CreationDate);
            Assert.AreEqual(SavedEntity.Votes.Count, LoadEntity.Votes.Count);
            Assert.AreEqual(SavedEntity.VideoLink, LoadEntity.VideoLink);
            Assert.AreEqual(SavedEntity.IsImageUploaded, LoadEntity.IsImageUploaded);
            Assert.AreEqual(SavedEntity.Answers.Count, LoadEntity.Answers.Count);
            Assert.AreEqual(SavedEntity.ImageReference, LoadEntity.ImageReference);
            Assert.AreEqual(SavedEntity.ThumbImageReference, LoadEntity.ThumbImageReference);
            Assert.AreEqual(SavedEntity.VotesDayScore, LoadEntity.VotesDayScore);
            Assert.AreEqual(SavedEntity.CommentsDayScore, LoadEntity.CommentsDayScore);
        }

        protected override Post CreateAndSaveEntity()
        {
            return new PostBuilder()
                            .WithComment(new CommentBuilder().Build())
                            .WithAnswer(new AnswerBuilder().Build())
                            .WithVote(new VoteBuilder().Build())
                            .BuildAndWriteToDatabase(Session);
        }
    }
}