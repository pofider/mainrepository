﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Persistence
{
    [TestFixture]
    public class CommentPersistenceTest : where_test_tests_entity_persistance<Comment>
    {
        [Test]
        public void TestPersistence()
        {
            SaveAndLoad();

            Assert.AreEqual(SavedEntity.Author, LoadEntity.Author);
            Assert.AreEqual(SavedEntity.Content, LoadEntity.Content);
            Assert.AreEqual(SavedEntity.Post, LoadEntity.Post);
            Assert.That(LoadEntity.CreationDate, Is.EqualTo(SavedEntity.CreationDate).Within(1000).Milliseconds);
        }

        protected override Comment CreateAndSaveEntity()
        {
            return new CommentBuilder().BuildAndWriteToDatabase(Session);
        }
    }
}