﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test.Persistence
{
    [TestFixture]
    public class VotePersistenceTest : where_test_tests_entity_persistance<Vote>
    {
        [Test]
        public void TestPersistence()
        {
            SaveAndLoad();

            Assert.AreEqual(SavedEntity.Author, LoadEntity.Author);
            Assert.AreEqual(SavedEntity.IsLike, LoadEntity.IsLike);
            Assert.AreEqual(SavedEntity.Post, LoadEntity.Post);
        }

        protected override Vote CreateAndSaveEntity()
        {
            return new VoteBuilder().BuildAndWriteToDatabase(Session);
        }
    }
}