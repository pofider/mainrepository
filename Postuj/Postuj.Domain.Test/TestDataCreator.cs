﻿using NUnit.Framework;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Test;

namespace Postuj.Domain.Test
{
    [TestFixture]
    [Ignore]
    public class TestDataCreator : where_smoke_test_access_database
    {
        [Test]
        public void Run()
        {
            new PostBuilder().WithContent("testovaci prispevek")
                .WithComment(new CommentBuilder().Build())
                .WithComment(new CommentBuilder().Build())
                .BuildAndWriteToDatabase(Session);

            new PostBuilder().WithContent("a jeste jeden")
               .BuildAndWriteToDatabase(Session);
        }
         
    }
}