﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Mapping
{
    public class PostMap : EntityMapBase<Post>
    {
         public PostMap()
         {
             Map(p => p.Content).CustomSqlType("nvarchar(500) COLLATE Czech_CI_AI");
             References(p => p.Author).Not.Nullable().Cascade.SaveUpdate();
             HasMany(p => p.Comments).Inverse().Cascade.AllDeleteOrphan();
             Map(p => p.Category).Not.Nullable();
             Map(p => p.Location).Not.Nullable();
             Map(p => p.CreationDate).Not.Nullable();
             HasMany(p => p.Votes).Inverse().Cascade.AllDeleteOrphan();
             HasMany(p => p.Answers).Inverse().Cascade.AllDeleteOrphan();
             Map(p => p.VideoLink);
             Map(p => p.OriginalVideoLink);
             Map(p => p.FacebookVideoLink);
             Map(p => p.VideoThumbLink);
             Map(p => p.VideoThumb);
             Map(p => p.VotesDayScore).Not.Nullable();
             Map(p => p.CommentsDayScore).Not.Nullable();
             Map(p => p.IsImageUploaded);
             Map(p => p.ContentWithoutDiacritics).Length(900);//.Index("idx_post_contentWithoutDiacritics");
             Component(c => c.ImageReference);
             Component(c => c.ThumbImageReference).ColumnPrefix("thumb");
         }
    }
}