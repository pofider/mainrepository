﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Mapping
{
    public class AnswerMap : EntityMapBase<Answer>
    {
         public AnswerMap()
         {
             References(a => a.Author).Cascade.SaveUpdate().Not.Nullable();
             References(a => a.Post).Cascade.SaveUpdate().Not.Nullable();
             Map(a => a.CreationDate).Not.Nullable();
         } 
    }
}