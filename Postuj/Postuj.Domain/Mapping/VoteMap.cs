﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Mapping
{
    public class VoteMap : EntityMapBase<Vote>
    {
        public VoteMap()
        {
            References(v => v.Author).Cascade.SaveUpdate().Not.Nullable();
            Map(v => v.IsLike).Not.Nullable();
            Map(v => v.CreationDate).Not.Nullable();
            References(v => v.Post).Cascade.SaveUpdate().Not.Nullable();
        }
    }
}