﻿using FluentNHibernate.Mapping;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Mapping
{
    public class FileReferenceMap : ComponentMap<FileReference>
    {
        public FileReferenceMap()
        {
            Map(p => p.UploadDate).Nullable();
            Map(p => p.FileName);
            Map(p => p.FileSize);
            Map(p => p.ContentType);
            Map(p => p.PublicUri);
            Map(p => p.PrivateUri);
        }
    }
}