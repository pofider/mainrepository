﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Mapping
{
    public class UserProfileMap : EntityMapBase<UserProfile>
    {
        public UserProfileMap()
        {
            Map(p => p.Provider);
            Map(p => p.ProviderUserId);
            Map(p => p.Username);
            Map(p => p.IsAdmin);
            Map(p => p.LastNotificationDisplayDate);
            Map(p => p.UsernameWithoutDiacritics).Index("idx_userprofile_usernameWithoutDiacritics");
        }
    }
}
