﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Mapping
{
    public class CommentMap : EntityMapBase<Comment>
    {
        public CommentMap()
        {
            Map(c => c.Content).Length(800).Not.Nullable();
            References(c => c.Author).Not.Nullable().Cascade.SaveUpdate();
            References(c => c.Post).Not.Nullable().Cascade.SaveUpdate();
            Map(c => c.CreationDate).Not.Nullable();
        }
    }
}
