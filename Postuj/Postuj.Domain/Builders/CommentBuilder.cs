﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class CommentBuilder : EntityBuilderBase<Comment>
    {
        private Post _post;
        private UserProfile _author;
        private string _content;
        private DateTime _creationTime = DateTime.Now;

        public CommentBuilder(bool setupDefaultReferences = true) : base(setupDefaultReferences)
        {
        }

        protected override Comment BuildEntity()
        {
            return new Comment(_content, _author, _post, _creationTime);
        }

        protected override void SetUpDefaultReferences()
        {
            _content = "content";
            _post = new PostBuilder().Build();
            _author = new UserProfileBuilder().Build();
        }

        public CommentBuilder WithPost(Post post)
        {
            _post = post;
            return this;
        }

        public CommentBuilder WithContent(string content)
        {
            _content = content;
            return this;
        }

        public CommentBuilder WithCreationDate(DateTime creationTime)
        {
            _creationTime = creationTime;
            return this;
        }


        public CommentBuilder WithAuthor(UserProfile author)
        {
            _author = author;
            return this;
        }
    }
}
