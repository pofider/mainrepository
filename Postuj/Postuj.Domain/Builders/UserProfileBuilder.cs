﻿using System;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class UserProfileBuilder : EntityBuilderBase<UserProfile>
    {
        private string _username;
        private string _provider;
        private string _providerUserId;
        private DateTime _lastNotificationDisplayDate = DateTime.Now;
        private bool _isAdmin = false;

        public UserProfileBuilder(bool setupDefaultReferences = true) : base(setupDefaultReferences)
        {
            
        }

        public UserProfileBuilder WithUsername(string username)
        {
            _username = username;
            return this;
        }

        public UserProfileBuilder WithIsAdmin(bool isAdmin)
        {
            _isAdmin = isAdmin;
            return this;
        }


        public UserProfileBuilder WithProvider(string provider)
        {
            _provider = provider;
            return this;
        }


        public UserProfileBuilder WithLastNotificationDisplayDate(DateTime lastNotificationDisplayDate)
        {
            _lastNotificationDisplayDate = lastNotificationDisplayDate;
            return this;
        }

        public UserProfileBuilder WithProviderUserId(string providerUserId)
        {
            _providerUserId = providerUserId;
            return this;
        }

        protected override void SetUpDefaultReferences()
        {
            _username = "Jan Blaha";
            _provider = "facebook";
            _providerUserId = "100003477016598";
        }

        protected override UserProfile BuildEntity()
        {
            return new UserProfile(_provider, _providerUserId, _username, _lastNotificationDisplayDate, _isAdmin);
        }
    }
}
