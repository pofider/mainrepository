﻿using System;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class AnswerBuilder : EntityBuilderBase<Answer>
    {
        private UserProfile _author;
        private Post _post;
        private DateTime _creationDate = DateTime.Now;

        public AnswerBuilder(bool setupDefaultReferences = true) : base(setupDefaultReferences)
        {
        }

        protected override void SetUpDefaultReferences()
        {
            _author = new UserProfileBuilder(true).Build();
            _post = new PostBuilder(true).Build();
        }

        public AnswerBuilder WithAuthor(UserProfile user)
        {
            _author = user;
            return this;
        }

        protected override Answer BuildEntity()
        {
            return new Answer(_author, _post, _creationDate);
        }

        public AnswerBuilder WithCreationDate(DateTime creationDate)
        {
            _creationDate = creationDate;
            return this;
        }

        public AnswerBuilder WithPost(Post post)
        {
            _post = post;
            return this;
        }
    }
}