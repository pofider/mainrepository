﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class PostBuilder : EntityBuilderBase<Post>
    {
        private string _content;
        private UserProfile _userProfile;
        private IList<Comment> _comments = new List<Comment>();
        private LocationEnum _location;
        private CategoryEnum _category;
        private DateTime _creationTime;
        private IList<Vote> _votes = new List<Vote>();
        private IList<Answer> _answers = new List<Answer>();
        private string _videoLink;
        private bool _isImageUploaded = false;
        private FileReference _thumbImageReference;
        private FileReference _imageReference;
        private long _votesDayScore = 0;
        private long _commentsDayScore = 0;
        private string _videoThumb;
        private string _originalVideoLink;
        private string _facebookVideoLink;
        private string _videouThumbLink;

        public PostBuilder(bool setupDefaultReferences = true) : base(setupDefaultReferences)
        {
        }

        public PostBuilder WithComment(Comment comment)
        {
            _comments.Add(comment);
            return this;
        }

        public PostBuilder WithCreationDate(DateTime creationDate)
        {
            _creationTime = creationDate;
            return this;
        }

        public PostBuilder WithContent(string content)
        {
            _content = content;
            return this;
        }

        public PostBuilder WithoutVotes()
        {
            _votes.Clear();
            return this;
        }

        public PostBuilder WithVote(Vote vote)
        {
            _votes.Add(vote);
            return this;
        }


        public PostBuilder WithAnswer(Answer answer)
        {
            _answers.Add(answer);
            return this;
        }

        public PostBuilder WithAuthor(UserProfile userProfile)
        {
            _userProfile = userProfile;
            return this;
        }

        public PostBuilder WithLocation(LocationEnum location)
        {
            _location = location;
            return this;
        }

        public PostBuilder WithCategory(CategoryEnum category)
        {
            _category = category;
            return this;
        }

        public PostBuilder WithVideoLink(string videoLink)
        {
            _videoLink = videoLink;
            return this;
        }

        public PostBuilder WithOriginalVideoLink(string videoLink)
        {
            _originalVideoLink = videoLink;
            return this;
        }

        public PostBuilder WithFacebookVideoLink(string videoLink)
        {
            _facebookVideoLink = videoLink;
            return this;
        }

        public PostBuilder WithVotesDayScore(long votesDayScore)
        {
            _votesDayScore = votesDayScore;
            return this;
        }

        public PostBuilder WithCommentsDayScore(long commentsDayScore)
        {
            _commentsDayScore = commentsDayScore;
            return this;
        }

        public PostBuilder WithImageReference(FileReference fileReference)
        {
            _imageReference = fileReference;
            return this;
        }


        public PostBuilder WithThumbImageReference(FileReference fileReference)
        {
            _thumbImageReference = fileReference;
            return this;
        }
        
        public PostBuilder WithVideoThumb(string thumb)
        {
            _videoThumb = thumb;
            return this;
        }

        protected override Post BuildEntity()
        {
            return new Post(_content, _userProfile, _comments, _category, _location, _creationTime, _votes, _videoLink, _isImageUploaded,
                _answers, _imageReference, _thumbImageReference, _votesDayScore, _commentsDayScore, _videoThumb, _originalVideoLink,
                _facebookVideoLink, _videouThumbLink);
        }

        protected override void SetUpDefaultReferences()
        {
            _content = "content";
            _videoLink = "http://youtube.com";
            _userProfile = new UserProfileBuilder().Build();
            _category = CategoryEnum.News;
            _location = LocationEnum.Praha;
            _creationTime = new DateTime(2012,01,01);
            _imageReference = new FileReferenceBuilder().Build();
            _thumbImageReference = new FileReferenceBuilder().Build();

            _votesDayScore = 5;
            _commentsDayScore = 6;
        }

        public PostBuilder WithVideoThumbLink(string thumb)
        {
            _videouThumbLink = thumb;
            return this;
        }
    }
}
