﻿using System;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class FileReferenceBuilder : EntityBuilderBase<FileReference>
    {
        private DateTime _uploadDateTtime = DateTime.Now;
        private string _fileName = "fileName";
        private string _contentType = "jpeg";
        private int _fileSize = 10;
        private string _publicUri = "";
        private string _privateUri = "";
        private string _uniqueId = "unique ID";
  
        public virtual FileReferenceBuilder WithUploadDate(DateTime uploadDate)
        {
            _uploadDateTtime = uploadDate;
            return this;
        }

        public FileReferenceBuilder WithFileName(string fileName)
        {
            _fileName = fileName;
            return this;
        }

        public FileReferenceBuilder WithFileSize(int fileSize)
        {
            _fileSize = fileSize;
            return this;
        }

        public FileReferenceBuilder WithContentType(string contentType)
        {
            _contentType = contentType;
            return this;
        }

        public FileReferenceBuilder WithPublicUri(string publicUri)
        {
            _publicUri = publicUri;
            return this;
        }

        public FileReferenceBuilder WithPrivateUri(string privateUri)
        {
            _privateUri = privateUri;
            return this;
        }


        public FileReferenceBuilder WithUniqueId(string uniqueId)
        {
            _uniqueId = uniqueId;
            return this;
        }

        protected override FileReference BuildEntity()
        {
            if (string.IsNullOrEmpty(_privateUri))
            {
                _privateUri = "postujcz\\" + _uniqueId;
                _publicUri = "api/Image/" + _uniqueId;
            }

            return new FileReference(_uploadDateTtime, _fileName, _contentType, _fileSize, _privateUri, _publicUri, _uniqueId);
        }
    }
}