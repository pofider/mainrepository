﻿using System;
using MFR.Infrastructure.Builders;
using Postuj.Domain.Domain;

namespace Postuj.Domain.Builders
{
    public class VoteBuilder : EntityBuilderBase<Vote>
    {
        private UserProfile _author;
        private bool _isLike;
        private Post _post;
        private DateTime _creationTime = DateTime.Now;

        public VoteBuilder(bool setupDefaultReferences = true) : base(setupDefaultReferences)
        {
        }

        public VoteBuilder WithIsLike(bool isLike)
        {
            _isLike = isLike;
            return this;
        }

        public VoteBuilder WithAuthor(UserProfile author)
        {
            _author = author;
            return this;
        }

        public VoteBuilder WithCreationDate(DateTime creationTime)
        {
            _creationTime = creationTime;
            return this;
        }

        public VoteBuilder WithPost(Post post)
        {
            _post = post;
            return this;
        }

        protected override Vote BuildEntity()
        {
            return new Vote(_author, _isLike, _post, _creationTime);
        }

        protected override void SetUpDefaultReferences()
        {
            _author = new UserProfileBuilder(true).Build();
            _isLike = true;
            _post = new PostBuilder().Build();
        }
        
    }
}