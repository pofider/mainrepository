﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Domain
{
    public class Comment : EntityBase
    {
        protected Comment()
        {
            
        }

        public Comment(string content, UserProfile author, Post post, DateTime creationDate)
        {
            Content = content;
            Author = author;
            Post = post;
            CreationDate = creationDate;
        }

        public virtual string Content { get; set; }
        public virtual UserProfile Author { get; protected set; }
        public virtual Post Post { get; protected set; }
        public virtual DateTime CreationDate { get; set; }

        public virtual void LinkToPost(Post post)
        {
            Post = post;
        }
    }
}
