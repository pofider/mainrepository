﻿using System;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Domain
{
    public class Answer : EntityBase
    {
        protected Answer() {}

        public Answer(UserProfile author, Post post, DateTime creationDate)
        {
            Author = author;
            Post = post;
            CreationDate = creationDate;
        }

        public virtual UserProfile Author { get; protected set; }
        public virtual Post Post { get; protected set; }
        public virtual DateTime CreationDate { get; protected set; }

        public virtual void LinkToPost(Post post)
        {
            Post = post;
        }
    }
}