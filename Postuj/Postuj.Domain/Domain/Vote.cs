﻿using System;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Domain
{
    public class Vote : EntityBase
    {
        public Vote(UserProfile author, bool isLike, Post post, DateTime creationTime)
        {
            Author = author;
            IsLike = isLike;
            Post = post;
            CreationDate = creationTime;
        }

        protected Vote()
        {
            
        }

        public virtual UserProfile Author { get; protected set; }
        public virtual bool IsLike { get; protected set; }
        public virtual Post Post { get; protected set; }
        public virtual DateTime CreationDate { get; protected set; }

        public virtual void LinkToPost(Post post)
        {
            Post = post;
        }
    }
}