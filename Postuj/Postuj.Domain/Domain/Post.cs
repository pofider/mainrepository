﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.DataAccess.ORM;

namespace Postuj.Domain.Domain
{
    public class Post : EntityBase
    {
        public const long EXPECTED_MAX_SCORE = 100000000;
        protected Post()
        {
            
        }

        public Post(string content, UserProfile author, IList<Comment> comments, CategoryEnum category, LocationEnum location, DateTime creationDate, 
            IList<Vote> votes, string videoLink, bool isImageUploaded, IList<Answer> answers, FileReference imageReference, FileReference thumbImageReference,
            long votesDayScore, long commentsDayScore, string videoThumb, string originalVideoLink, string facebookVideoLink, string videoThumbLink)
        {
            Content = content;
            Author = author;
            Comments = comments;
            Category = category;
            Location = location;
            CreationDate = creationDate;
            Votes = votes;
            Answers = answers;
            VideoLink = videoLink;
            IsImageUploaded = isImageUploaded;
            ImageReference = imageReference;
            ThumbImageReference = thumbImageReference;
            ContentWithoutDiacritics = content.ReplaceDiacritics();
            VotesDayScore = votesDayScore;
            CommentsDayScore = commentsDayScore;
            VideoThumb = videoThumb;
            OriginalVideoLink = originalVideoLink;
            FacebookVideoLink = facebookVideoLink;
            VideoThumbLink = videoThumbLink;

            if (Comments != null)
                Comments.ToList().ForEach(c => c.LinkToPost(this));

            if (Votes != null)
                Votes.ToList().ForEach(v => v.LinkToPost(this));

            if (Answers != null)
                Answers.ToList().ForEach(a => a.LinkToPost(this));
        }

        public virtual string VideoThumbLink { get; set; }

        private string _content;
        public virtual string Content { 
            get { return _content; }
            set { _content = value; if (value != null)
                ContentWithoutDiacritics = value.ReplaceDiacritics();
            }
        }
        public virtual string ContentWithoutDiacritics { get; set; }
        public virtual UserProfile Author { get; set; }
        public virtual IList<Comment> Comments { get; set; }
        public virtual CategoryEnum Category { get; set; }
        public virtual LocationEnum Location { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual bool IsImageUploaded { get; set; }
        public virtual FileReference ImageReference { get; set; }
        public virtual FileReference ThumbImageReference { get; set; }
        public virtual IList<Vote> Votes { get; protected set; }
        public virtual IList<Answer> Answers { get; protected set; }
        public virtual string VideoLink { get; protected set; }
        public virtual string OriginalVideoLink { get; protected set; }
        public virtual string FacebookVideoLink { get; protected set; }
        public virtual string VideoThumb { get; protected set; }
        
        public virtual long VotesDayScore { get; protected set; }
        public virtual long CommentsDayScore { get; protected set; }

        public virtual string GetPublicUri()
        {
            return ConfigurationManager.AppSettings["BaseUrl"] + ImageReference.PublicUri;
        }

        public virtual string GetPublicThumbUri()
        {
            if (ImageReference == null)
                return "";

            return ConfigurationManager.AppSettings["BaseUrl"] + ImageReference.PublicUri;
        }

        public virtual void AddVote(Vote vote)
        {
            vote.LinkToPost(this);
            Votes.Add(vote);
            VotesDayScore = Votes.Count() + (EXPECTED_MAX_SCORE * (long) TimeSpan.FromTicks(DateTime.Now.Ticks).TotalDays);
        }

        public virtual void AddComment(Comment comment)
        {
            comment.LinkToPost(this);
            Comments.Add(comment);
            CommentsDayScore = Comments.Count() + (EXPECTED_MAX_SCORE * (long)TimeSpan.FromTicks(DateTime.Now.Ticks).TotalDays);
        }

        public virtual void AddAnswer(Answer answer)
        {
            answer.LinkToPost(this);
            Answers.Add(answer);
        }

        public virtual void RemoveAnswer(Answer answer)
        {
            Answers.Remove(answer);
        }

        public virtual void RemoveComment(Comment comment)
        {
            Comments.Remove(comment);
            CommentsDayScore = Comments.Count() * (long)TimeSpan.FromTicks(DateTime.Now.Ticks).TotalDays;
        }
    }
}