﻿using System;

namespace Postuj.Domain.Domain
{
    public enum LocationEnum
    {
         NotSpecified = 0,
         Praha = 1,
         Zlinsky = 2,
         Plzensky = 3,
         KraloveHradecky = 4,
         Pardubicky = 5,
         Jihocesky = 6,
         Vysocina = 7,
         JihoMoravsky = 8,
         Karlovarsky = 9,
         Ustecky = 10,
         MoravskoSlezsky = 11,
         Liberecky = 12,
         Olomoucky = 13,
         Stredocesky = 14,
    }

    public static class LocationEnumExtension
    {
        public static LocationEnum? ToLocationEnum(this string code)
        {
            switch (code)
            {
                case "null":
                case "undefined":
                case null:
                case "": return LocationEnum.NotSpecified;
                case "all": return LocationEnum.NotSpecified;
                case "praha": return LocationEnum.Praha;
                case "zlinsky": return LocationEnum.Zlinsky;
                case "plzensky": return LocationEnum.Plzensky;
                case "kralovehradecky" : return LocationEnum.KraloveHradecky;
                case "pardubicky" : return LocationEnum.Pardubicky;
                case "jihocesky" : return LocationEnum.Jihocesky;
                case "vysocina" : return LocationEnum.Vysocina;
                case "jihomoravsky" : return LocationEnum.JihoMoravsky;
                case "karlovarsky" : return LocationEnum.Karlovarsky;
                case "ustecky" : return LocationEnum.Ustecky;
                case "moravskoslezsky" : return LocationEnum.MoravskoSlezsky;
                case "liberecky" : return LocationEnum.Liberecky;
                case "olomoucky" : return LocationEnum.Olomoucky;
                case "stredocesky": return LocationEnum.Stredocesky;
            }

            throw new InvalidOperationException("unsuported type");
        }
    }
}