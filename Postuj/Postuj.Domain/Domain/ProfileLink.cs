﻿namespace Postuj.Domain.Domain
{
    public class ProfileLink
    {
        public ProfileLink(UserProfile profile)
            : this(profile.Provider, profile.ProviderUserId)
        {
        }

        public ProfileLink(string provider, string providerUserId)
        {
            Url = string.Format("http://www.facebook.com/people/@/{0}", providerUserId);
        }

        public string Url { get; private set; }
    }
}