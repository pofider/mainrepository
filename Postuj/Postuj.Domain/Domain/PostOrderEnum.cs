﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postuj.Domain.Domain
{
    public enum PostOrderEnum
    {
        NotSpecified = 0,
        Likes = 1,
        Comments = 2,
        LikesAll = 3,
        CommentsAll = 4
    }

    public static class PostOrderEnumExtension
    {
        private static IDictionary<string, PostOrderEnum> _map = new Dictionary<string, PostOrderEnum>()
                                                                    {
                                                                        {"likes", PostOrderEnum.Likes},
                                                                        {"comments", PostOrderEnum.Comments},
                                                                        {"likes-all", PostOrderEnum.LikesAll},
                                                                        {"comments-all", PostOrderEnum.CommentsAll},
                                                                        {"", PostOrderEnum.NotSpecified},
                                                                        {"null", PostOrderEnum.NotSpecified}
                                                                    };

        public static PostOrderEnum? ToPostOrderEnum(this string code)
        {
            if (_map.ContainsKey(code))
                return _map[code];

            return null;
        }

        public static string ToPostOrderEnumCode(this PostOrderEnum order)
        {
            return _map.Where(kp => kp.Value == order).Select(kp => kp.Key).Single();
        }
    }
}
