﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.DataAccess.ORM;
using Postuj.Infrastructure.Security;

namespace Postuj.Domain.Domain
{
    [Serializable]
    public class UserProfile : EntityBase, IPrincipal, IUser, IIdentity
    {
        [NonSerialized]
        [ThreadStatic] public static UserProfile Current;

        protected UserProfile()
        {
            
        }

        public UserProfile(string provider, string providerUserId, string username, DateTime lastNotificationDisplayDate, bool isAdmin)
        {
            Provider = provider;
            ProviderUserId = providerUserId;
            Username = username;
            LastNotificationDisplayDate = lastNotificationDisplayDate;
            IsAdmin = isAdmin;
            UsernameWithoutDiacritics = Username.ReplaceDiacritics();
        }

        public virtual string Provider { get; protected set; }
        public virtual string ProviderUserId { get; protected set; }

        private string _username;
        public virtual string Username
        {
            get { return _username; }
            protected set { _username = value;
                if (value != null) UsernameWithoutDiacritics = value.ReplaceDiacritics();
            }
        }
        
        public virtual bool IsAdmin { get; set; }
        public virtual string UsernameWithoutDiacritics { get; set; }

        public virtual ProfileImage ProfileImage
        {
            get { return new ProfileImage(this); }
        }
        
        public virtual ProfileLink ProfileLink
        {
            get { return new ProfileLink(this); }
        }

        public virtual DateTime LastNotificationDisplayDate { get; set; }

        public virtual bool IsInRole(string role)
        {
            return true;
        }
        
        public virtual IIdentity Identity
        {
            get { return this; }
        }

        public virtual string Name
        {
            get { return Username; }
        }

        public virtual string AuthenticationType { get; protected set; }
        public virtual bool IsAuthenticated { get; set; }
    }
}
