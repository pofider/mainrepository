﻿using System;

namespace Postuj.Domain.Domain
{

    public class FileReference
    {
        public FileReference() { }

        public FileReference(DateTime uploadDate, string fileName, string contentType, int fileSize, string privateUri, string publicUri, string uniqueId)
        {
            UploadDate = uploadDate;
            FileName = fileName;
            ContentType = contentType;
            FileSize = fileSize;
            PublicUri = publicUri;
            PrivateUri = privateUri;
            UniqueId = uniqueId;
        }


        public virtual DateTime? UploadDate { get; set; }
        public virtual string FileName { get; set; }
        public virtual string PublicUri { get; set; }
        public virtual string PrivateUri { get; set; }
        public virtual string ContentType { get; set; }
        public virtual int FileSize { get; set; }
        public virtual string UniqueId { get; set; }

        protected bool Equals(FileReference other)
        {
            return string.Equals(PrivateUri, other.PrivateUri);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FileReference)obj);
        }

        public override int GetHashCode()
        {
            return (PrivateUri != null ? PrivateUri.GetHashCode() : 0);
        }
    }
    //public class AttachedImageLink
    //{
    //    public AttachedImageLink(Post post) : this(post.Id.Value)
    //    {
    //    }

    //    public AttachedImageLink(long postId)
    //    {
    //        Url = string.Format("http://postujcz.blob.core.windows.net/postujcz/image-{0}", postId);
    //        ThumbUrl = string.Format("http://postujcz.blob.core.windows.net/postujcz/thumb-{0}", postId);

    //        StoragePath = string.Format("postujcz\\image-{0}", postId);
    //        StorageTumbPath = string.Format("postujcz\\thumb-{0}", postId);
    //    }

    //    public virtual string StoragePath { get; private set; }
    //    public virtual string StorageTumbPath { get; private set; }
    //    public virtual string Url { get; private set; }
    //    public virtual string ThumbUrl { get; private set; }
    //}
}