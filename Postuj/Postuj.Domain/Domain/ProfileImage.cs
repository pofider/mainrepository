﻿namespace Postuj.Domain.Domain
{
    public class ProfileImage
    {
        public ProfileImage(UserProfile profile) : this(profile.Provider, profile.ProviderUserId)
        {
        }

        public ProfileImage(string provider, string providerUserId)
        {
            Url = string.Format("https://graph.facebook.com/{0}/picture", providerUserId);
        }

        public string Url { get; private set; }
    }
}