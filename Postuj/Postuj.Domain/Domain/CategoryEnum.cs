﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Postuj.Domain.Domain
{
    public enum CategoryEnum
    {
        NotSpecified = 0,
        News = 1,
        Media = 2,
        Meetings = 3,
        Events = 4,
        Video = 5,
    }

    public static class CategoryEnumExtension
    {
        private static IDictionary<string, CategoryEnum> _map = new Dictionary<string, CategoryEnum>()
                                                                    {
                                                                        {"news", CategoryEnum.News},
                                                                        {"meetings", CategoryEnum.Meetings},
                                                                        {"media", CategoryEnum.Media},  
                                                                        {"events", CategoryEnum.Events},
                                                                        {"videos", CategoryEnum.Video},
                                                                        {"", CategoryEnum.NotSpecified},
                                                                        {"null", CategoryEnum.NotSpecified},
                                                                        {"undefined", CategoryEnum.NotSpecified}
                                                                    };  

        public static CategoryEnum? ToCategoryEnum(this string code)
        {
            if (_map.ContainsKey(code))
                return _map[code];

            return null;
        }

        public static string ToCategoryEnumCode(this CategoryEnum category)
        {
            return _map.Where(kp => kp.Value == category).Select(kp => kp.Key).Single();
        }
    }
}