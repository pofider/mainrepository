﻿using System;

namespace Postuj.Domain.Shared
{
    public static class DateTimeHelper
    {
         public static string ToTimeFromNowString(this DateTime date)
         {
             var fromNow = DateTime.Now - date;

             if (fromNow.TotalHours > 48)
                 return date.ToString("dd.MM.");

             if (fromNow.TotalHours > 24)
                 return "včera";

             if (fromNow.TotalHours > 4)
                 return Math.Round(fromNow.TotalHours) + " hodin";

             if (fromNow.TotalHours > 1)
                 return Math.Round(fromNow.TotalHours) + " hodiny";


             if (fromNow.TotalMinutes > 59)
                 return "1 hodina";

             if (fromNow.TotalMinutes < 1)
                 return "1 minuta";

             if (fromNow.TotalMinutes < 5)
                 return Math.Round(fromNow.TotalMinutes) + " minuty";

             return Math.Round(fromNow.TotalMinutes) + " minut";
         }
    }
}