﻿using System;

namespace Postuj.Domain.Shared
{
    public static class EnumHelper
    {
         public static string ToEnumString(this object enumValue)
         {
             var valueName = Enum.GetName(enumValue.GetType(), enumValue);
             return PostujResources.ResourceManager.GetString(string.Format("{0}_{1}", enumValue.GetType().Name, valueName));
         }
    }
}