﻿using System.Collections.Generic;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Query;
using System.Linq;

namespace Postuj.Domain.Query
{
    public class NewNotificationCompositeQuery : PostujQueryBase<object>
    {
        public override IEnumerable<object> Execute()
        {
            var voteNotifications = Query(new VoteNotificationQuery()
                                              {
                                                  SinceDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
                                              });
            var commentNotifications = Query(new CommentNotificationQuery()
                                                 {
                                                     SinceDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
                                                 });

            var answerNotifications = Query(new AnswerNotificationQuery()
            {
                SinceDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
            });


            var notifications = voteNotifications.Union(commentNotifications).Union(answerNotifications);

            return WithPaging(notifications.OrderByDescending(x => x.GetType().GetProperty("CreationDate").GetValue(x)));
        }
    }
}