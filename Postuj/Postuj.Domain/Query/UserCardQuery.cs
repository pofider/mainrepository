﻿using System;
using System.Linq;
using NHibernate.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class UserCardQuery : PostujQueryBase<object>
    {
        public override object ExecuteSingle()
        {
            if (CurrentUser == null)
                return null;

            var myPosts = Session.Query<Post>().Count(p => p.Author.Id == CurrentUser.Id);
            var myAnswers = Session.Query<Answer>().Count(p => p.Author.Id == CurrentUser.Id);
            var notifications = Query(new NewNotificationCompositeQuery() { PageNumber = null }).Count();
            

            return new
                       {
                           PostCount = myPosts,
                           AnswerCount = myAnswers,
                           NotificationCount = notifications
                       };
        }
    }
}