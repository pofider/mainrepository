﻿using System.Collections.Generic;
using System.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class OldNotificationCompositeQuery : PostujQueryBase<object>
    {
        public override IEnumerable<object> Execute()
        {
            var voteNotifications = Query(new VoteNotificationQuery()
                                              {
                                                  PageNumber = null,
                                                  BeforeDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
                                              });

            var commentNotifications = Query(new CommentNotificationQuery()
                                                 {
                                                     PageNumber = null,
                                                     BeforeDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
                                                 });

            var answerNotifications = Query(new AnswerNotificationQuery()
            {
                PageNumber = null,
                BeforeDate = ((UserProfile)CurrentUser).LastNotificationDisplayDate
            });

            var notifications = voteNotifications.Union(commentNotifications).Union(answerNotifications);

            return WithPaging(notifications.OrderByDescending(x => x.GetType().GetProperty("CreationDate").GetValue(x)));
        }
    }
}