﻿using System;
using System.Collections.Generic;
using System.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class AnswerNotificationQuery : PostujGenericQueryOverBase<Answer, object>
    {
        public const string ANSWER_MEETING_NOTIFICATION = "ANSWER_MEETING";
        public const string ANSWER_EVENT_NOTIFICATION = "ANSWER_EVENT";

        public DateTime? SinceDate { get; set; }
        public DateTime? BeforeDate { get; set; }

        protected override IEnumerable<object> InnerExecute()
        {
            Answer answerAlias = null;
            var query = Session.QueryOver<Answer>(() => answerAlias);

            query = query.Where(a => a.Author.Id != CurrentUser.Id);

            UserProfile userAlias = null;
            query = query.JoinAlias(() => answerAlias.Author, () => userAlias);

            Post postAlias = null;
            query = query.JoinAlias(() => answerAlias.Post, () => postAlias);

            UserProfile postAuthor = null;
            query = query.JoinAlias(() => answerAlias.Post.Author, () => postAuthor);

            query = query.Where(v => postAuthor.Id == CurrentUser.Id);

            if (SinceDate.HasValue)
                query = query.Where(v => v.CreationDate >= SinceDate);

            if (BeforeDate.HasValue)
                query = query.Where(v => v.CreationDate < BeforeDate);

            query = query.SelectList(list => list
                .Select(v => userAlias.Username)
                .Select(v => postAlias.Content)
                .Select(v => v.CreationDate)
                .Select(v => v.Id)
                .Select(v => postAlias.Id)
                .Select(v => userAlias.Provider)
                .Select(v => userAlias.ProviderUserId)
                .Select(v => postAlias.Category));

            query = query.OrderBy(v => v.CreationDate).Desc;

            return WithPaging(query).List<object[]>().Select(l =>
                new
                {
                    Username = l[0],
                    Content = ((string)l[1]).Shorten(),
                    CreationDate = l[2],
                    Id = l[3],
                    PostId = l[4],
                    Type = ((CategoryEnum)l[7]) == CategoryEnum.Events ? ANSWER_EVENT_NOTIFICATION : ANSWER_MEETING_NOTIFICATION,
                    ProfileLink = new ProfileLink((string)l[5], (string)l[6]).Url,
                });
        }
    }
}