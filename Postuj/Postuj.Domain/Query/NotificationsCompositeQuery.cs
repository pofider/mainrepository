﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class NotificationsCompositeQuery : PostujQueryBase<object>
    {
        public override object ExecuteSingle()
        {
            var result = new
                {
                    New = Query(new NewNotificationCompositeQuery() {PageNumber = null}),
                    Old = Query(new OldNotificationCompositeQuery() {PageNumber = PageNumber})
                };

            ((UserProfile) CurrentUser).LastNotificationDisplayDate = DateTime.Now;
            Session.SaveOrUpdate(CurrentUser);

            return result;
        }
    }
}
