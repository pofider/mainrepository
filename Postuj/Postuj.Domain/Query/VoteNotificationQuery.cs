﻿using System;
using System.Collections.Generic;
using System.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class VoteNotificationQuery : PostujGenericQueryOverBase<Vote, object>
    {
        public const string VOTE_NOTIFICATION = "VOTE";

        public DateTime? SinceDate { get; set; }
        public DateTime? BeforeDate { get; set; }

        protected override IEnumerable<object> InnerExecute()
        {
            Vote voteAlias = null;
            var query = Session.QueryOver<Vote>(() => voteAlias);

            query = query.Where(v => v.Author.Id != CurrentUser.Id);
            
            UserProfile userAlias = null;
            query = query.JoinAlias(() => voteAlias.Author, () => userAlias);

            Post postAlias = null;
            query = query.JoinAlias(() => voteAlias.Post, () => postAlias);
           
            UserProfile postAuthor = null;
            query = query.JoinAlias(() => voteAlias.Post.Author, () => postAuthor);

            query = query.Where(v => postAuthor.Id == CurrentUser.Id);

            if (SinceDate.HasValue)
                query = query.Where(v => v.CreationDate >= SinceDate);

            if (BeforeDate.HasValue)
                query = query.Where(v => v.CreationDate < BeforeDate);

            query = query.SelectList(list => list
                .Select(v => userAlias.Username)
                .Select(v => v.IsLike)
                .Select(v => postAlias.Content)
                .Select(v => v.CreationDate)
                .Select(v => v.Id)
                .Select(v => postAlias.Id)
                .Select(v => userAlias.Provider)
                .Select(v => userAlias.ProviderUserId));

            query = query.OrderBy(v => v.CreationDate).Desc;

            return WithPaging(query).List<object[]>().Select(l => 
                new
                    {
                        Username = l[0],
                        IsLike = l[1],
                        Content = ((string)l[2]).Shorten(),
                        CreationDate = l[3],
                        Id = l[4],
                        PostId = l[5],
                        ProfileLink = new ProfileLink((string) l[6], (string) l[7]).Url,
                        Type = VOTE_NOTIFICATION
                    });
        }
    }
}