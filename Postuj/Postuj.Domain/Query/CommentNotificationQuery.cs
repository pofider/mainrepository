﻿using System;
using System.Collections.Generic;
using System.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class CommentNotificationQuery : PostujGenericQueryOverBase<Comment, object>
    {
        public const string COMMENT_NOTIFICATION = "COMMENT";

        public DateTime? SinceDate { get; set; }
        public DateTime? BeforeDate { get; set; }

        protected override IEnumerable<object> InnerExecute()
        {
            Comment commentAlias = null;
            var query = Session.QueryOver<Comment>(() => commentAlias);

            query = query.Where(c => c.Author.Id != CurrentUser.Id);

            UserProfile userAlias = null;
            query = query.JoinAlias(() => commentAlias.Author, () => userAlias);

            Post postAlias = null;
            query = query.JoinAlias(() => commentAlias.Post, () => postAlias);

            UserProfile postAuthor = null;
            query = query.JoinAlias(() => commentAlias.Post.Author, () => postAuthor);

            query = query.Where(c => postAuthor.Id == CurrentUser.Id);

            if (SinceDate.HasValue)
                query = query.Where(v => v.CreationDate >= SinceDate);

            if (BeforeDate.HasValue)
                query = query.Where(v => v.CreationDate < BeforeDate);

            query = query.SelectList(list => list
                .Select(c => userAlias.Username)
                .Select(c => c.Content)
                .Select(c => postAlias.Content)
                .Select(c => c.CreationDate)
                .Select(c => c.Id)
                .Select(c => postAlias.Id)
                .Select(v => userAlias.Provider)
                .Select(v => userAlias.ProviderUserId));

            query = query.OrderBy(c => c.CreationDate).Desc;

            return WithPaging(query).List<object[]>().Select(l =>
                new
                {
                    Username = l[0],
                    Content = ((string)l[1]).Shorten(),
                    PostContent = l[2],
                    CreationDate = l[3],
                    Id = l[4],
                    PostId = l[5],
                    Type = COMMENT_NOTIFICATION,
                    ProfileLink = new ProfileLink((string)l[6], (string)l[7]).Url,
                });
        }
    }
}