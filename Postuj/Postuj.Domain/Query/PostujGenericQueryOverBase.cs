﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.Query;
using Postuj.Infrastructure.Security;

namespace Postuj.Domain.Query
{
    public abstract class PostujGenericQueryOverBase<TEntity, TResult> : GenericQueryOverBase<TEntity, TResult>  where TEntity : class
    {
        protected UserProfile CurrentPostujUser
        {
            get { return CurrentUser as UserProfile; }
        }

        public override IUser CurrentUser
        {
            get
            {
                var baseResult = base.CurrentUser as UserProfile;

                if (baseResult != null)
                    return baseResult;

                return UserProfile.Current;
            }
            set
            {
                UserProfile.Current = value as UserProfile;
                base.CurrentUser = value;
            }
        }
    }
}