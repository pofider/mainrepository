﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Domain.Shared;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class CommentListQuery : PostujGenericQueryOverBase<Comment, CommentListDto>
    {
        public long? PostId { get; set; }
        public long? CommentId { get; set; }
        public long[] PostIds { get; set; }

        public bool? Descending { get; set; }

        protected override IEnumerable<CommentListDto> InnerExecute()
        {
            Comment comment = null;
            var mainQuery = Session.QueryOver<Comment>(() => comment);

            if (Descending != false)
                mainQuery = mainQuery.OrderBy(p => p.CreationDate).Asc;
            else
                mainQuery = mainQuery.OrderBy(p => p.CreationDate).Desc;

            if (PostIds != null)
                mainQuery = mainQuery.Where(c => c.Post.Id.Value.IsIn(PostIds));

            if (PostId.HasValue)
                mainQuery = mainQuery.Where(c => c.Post.Id == PostId);

            if (CommentId.HasValue)
                mainQuery = mainQuery.Where(c => c.Id == CommentId);

            UserProfile commentAuthor = null;
            mainQuery = mainQuery.JoinAlias(() => comment.Author, () => commentAuthor);

            var result = WithPaging(mainQuery.SelectList(
                list => list
                            .Select(p => p.Content)
                            .Select(p => p.Author.Id)
                            .Select(p => p.CreationDate)
                            .Select(p => p.Id)
                            .Select(p => commentAuthor.Username)
                            .Select(p => commentAuthor.Provider)
                            .Select(p => commentAuthor.ProviderUserId)
                )).Future<object[]>().Select(l => new CommentListDto
                {
                    Content = (string) l[0],
                    Author = (long) l[1],
                    CreationDate = (DateTime) l[2],
                    Id = (long) l[3],
                    CreationDateString = TimeZoneInfo.ConvertTime((DateTime)l[2], TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time")).ToString("dd.MM.yyyy HH:mm"),
                    Username = (string) l[4],
                    ProfileImageLink = new ProfileImage((string)l[5], (string) l[6]).Url,
                    ProfileLink = new ProfileLink((string)l[5], (string)l[6]).Url
                });

            if (Descending == false)
                return result.OrderBy(c => c.CreationDate);

            return result;
        }

        public override IEnumerable<CommentListDto> Invariant(IEnumerable<CommentListDto> results)
        {
            foreach (CommentListDto result in results)
            {
                result.CreationDateString =
                    TimeZoneInfo.ConvertTime((DateTime)result.CreationDate,
                                             TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"))
                                .ToString("dd.MM.yyyy HH:mm");
            }

            return results;
        }
    }
}