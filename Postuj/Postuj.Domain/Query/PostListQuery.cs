﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Microsoft.WindowsAzure.Diagnostics;
using NHibernate.Criterion;
using NHibernate.Linq;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Domain.Shared;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class PostListQuery : PostujGenericQueryOverBase<Post, PostListDto>
    {
        public CategoryEnum? Category { get; set; }
        public string CategoryCode { get; set; }
        public LocationEnum? Location { get; set; }
        public string LocationCode { get; set; }
        public string PostOrderCode { get; set; }
        public PostOrderEnum? PostOrder { get; set; }
        public UserProfile PostedBy { get; set; }
        public UserProfile CommentedBy { get; set; }
        public UserProfile AnsweredBy { get; set; }
        public bool? AnsweredByMe { get; set; }
        public string SearchString { get; set; }
        public bool SupressUnfinishedImagesFiltering { get; set; }

        public bool? PostedByMe { get; set; }

        public long? PostId { get; set; }

        public override string CacheKey
        {
            get
            {
                return string.Format("PostListQuery-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}", CategoryCode, LocationCode,
                                     PostOrderCode, PostedBy != null ? PostedBy.Id.ToString() : "",
                                     CommentedBy != null ? CommentedBy.Id.ToString() : "",
                                     AnsweredBy != null ? AnsweredBy.Id.ToString() : "", AnsweredByMe, SearchString, PostedByMe,
                                     PostId, PageNumber);
            }
        }

        protected override IEnumerable<PostListDto> InnerExecute()
        {
            if (Category == null && CategoryCode != null)
                Category = CategoryCode.ToCategoryEnum();

            if (Location == null && LocationCode != null)
                Location = LocationCode.ToLocationEnum();

            if (PostOrder == null && PostOrderCode != null)
                PostOrder = PostOrderCode.ToPostOrderEnum();

            if (PostedByMe == true)
                PostedBy = (UserProfile) CurrentUser;

            if (AnsweredByMe == true)
                AnsweredBy = (UserProfile) CurrentUser;

            Vote vote = null;
            Comment comment = null;
            Post post = null;
            Answer answer = null;
            Answer myAnswer = null;
            
            var likesSubQuery = QueryOver.Of(() => vote).Where(() => vote.Post.Id == post.Id && vote.IsLike).ToRowCountQuery();
            var disLikesSubQuery = QueryOver.Of(() => vote).Where(() => vote.Post.Id == post.Id && !vote.IsLike).ToRowCountQuery();
            var commentsSubQuery = QueryOver.Of(() => comment).Where(() => comment.Post.Id == post.Id).ToRowCountQuery();
            var answersSubQuery = QueryOver.Of(() => answer).Where(() => answer.Post.Id == post.Id).ToRowCountQuery();

            var currentUserId = CurrentUser == null ? 0 : CurrentUser.Id;
            var myAnswerSubQuery = QueryOver.Of(() => myAnswer).Where(() =>
                myAnswer.Post.Id == post.Id && myAnswer.Author.Id == currentUserId).ToRowCountQuery();

            var mainQuery = Session.QueryOver(() => post);

            if (Category != null && Category != CategoryEnum.NotSpecified)
                mainQuery = mainQuery.Where(p => p.Category == Category);

            if (Location != null && Location != LocationEnum.NotSpecified)
                mainQuery = mainQuery.Where(p => p.Location == Location);

            if (PostedBy != null)
                mainQuery = mainQuery.Where(p => p.Author == PostedBy);

            if (AnsweredBy != null)
            {
                mainQuery = mainQuery.WithSubquery.WhereExists(QueryOver.Of<Answer>()
                                                                        .Where(a => a.Author.Id == CurrentUser.Id)
                                                                        .Where(a => a.Post.Id == post.Id).Select(a => a.Id));
            }

            if (PostId.HasValue)
            {
                mainQuery = mainQuery.Where(p => p.Id == PostId);
            }

            if (CommentedBy != null)
            {
                mainQuery = mainQuery.WithSubquery.WhereExists(QueryOver.Of<Comment>()
                    .Where(c => c.Author.Id == CommentedBy.Id)
                    .Where(c => c.Post.Id == post.Id)
                    .Select(c => c.Id));
            }
            
            UserProfile postAuthor = null;
            mainQuery = mainQuery.JoinAlias(() => post.Author, () => postAuthor);

            if (!SupressUnfinishedImagesFiltering)
                mainQuery = mainQuery.Where(p => p.Category != CategoryEnum.Media || p.IsImageUploaded);

            if (!string.IsNullOrEmpty(SearchString) && SearchString != "null")
            {
                mainQuery = mainQuery
                    .Where(p =>
                        postAuthor.UsernameWithoutDiacritics.IsLike(string.Format("%{0}%", SearchString).ToLower().ReplaceDiacritics()) ||
                        p.ContentWithoutDiacritics.IsLike(string.Format("%{0}%", SearchString).ToLower().ReplaceDiacritics()));
            }

            object commentsAlias = null;
            object likesAlias = null;

            mainQuery = mainQuery.SelectList(
                list => list
                            .Select(p => p.Content)
                            .Select(p => p.Author.Id)
                            .SelectSubQuery(likesSubQuery).WithAlias(() => likesAlias)
                            .SelectSubQuery(disLikesSubQuery)
                            .SelectSubQuery(commentsSubQuery).WithAlias(() => commentsAlias)
                            .Select(p => p.Location)
                            .Select(p => p.Category)
                            .Select(p => p.CreationDate)
                            .Select(p => p.Id)
                            .Select(p => postAuthor.Username)
                            .Select(p => postAuthor.Provider)
                            .Select(p => postAuthor.ProviderUserId)
                            .SelectSubQuery(answersSubQuery)
                            .Select(p => p.ImageReference.PublicUri)
                            .Select(p => p.ThumbImageReference.PublicUri)
                            .Select(p => p.VideoLink)
                            .SelectSubQuery(myAnswerSubQuery)
                            .Select(p => p.VideoThumb)
                );

            if (PostOrder == PostOrderEnum.Comments)
                mainQuery = mainQuery.OrderBy(p => p.CommentsDayScore).Desc;

            if (PostOrder == PostOrderEnum.Likes)
                mainQuery = mainQuery.OrderBy(p => p.VotesDayScore).Desc;

            if (PostOrder == PostOrderEnum.LikesAll)
                mainQuery = mainQuery.OrderBy(Projections.SubQuery(likesSubQuery)).Desc;

            if (PostOrder == PostOrderEnum.CommentsAll)
                mainQuery = mainQuery.OrderBy(Projections.SubQuery(commentsSubQuery)).Desc;

            mainQuery = mainQuery.OrderBy(p => p.CreationDate).Desc;
            
            var posts = WithPaging(mainQuery).Future<object[]>();

            //var postIds = posts.Select(l => (long) l[8]).ToArray();


            return posts.Select(l => new PostListDto
                    {
                        Content = (string) l[0],
                        Author = (long) l[1],
                        Likes = (int) l[2],
                        Dislikes = (int) l[3],
                        Comments = (int) l[4],
                        Location = (LocationEnum) l[5],
                        CategoryCode = ((CategoryEnum)l[6]).ToCategoryEnumCode(),
                        CreationDate = (DateTime) l[7],
                        Id = (long) l[8],
                        LocationString = l[5].ToEnumString(),
                        CategoryString = l[6].ToEnumString(),
                        CreationDateString = ((DateTime)l[7]).ToTimeFromNowString(),
                        Username = (string) l[9],
                        ProfileImageLink = new ProfileImage((string)l[10], (string)l[11]).Url,
                        ProfileLink = new ProfileLink((string)l[10], (string)l[11]).Url,
                        AttachedImageLink = (string) l[13],
                        AttachedImageThumbLink = (string) l[14],
                        VideoLink = (string) l[15],
                        VideoThumb = (string) l[17],
                        Answers = (int) l[12],
                        HasMyAnswer = l[16] != null && ((int)l[16]) == 1,
                        CommentsList = Query(new CommentListQuery()
                            {
                                PageNumber = 1,
                                PageSize = 3,
                                PostId = (long) l[8],
                                Descending = false,
                            })
                    }
                );
        }

        public override IEnumerable<PostListDto> Invariant(IEnumerable<PostListDto> results)
        {
            foreach (PostListDto result in results)
            {
                result.CreationDateString = ((DateTime) result.CreationDate).ToTimeFromNowString();
            }

            return results;
        }
    }
}
