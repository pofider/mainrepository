﻿using System;
using System.Collections.Generic;
using System.Linq;
using Postuj.Domain.Domain;
using Postuj.Domain.Shared;
using Postuj.Infrastructure.Query;

namespace Postuj.Domain.Query
{
    public class AnswerListQuery : PostujGenericQueryOverBase<Answer, object>
    {
        public long? PostId { get; set; }
        
        protected override IEnumerable<object> InnerExecute()
        {
            //ValidatePostForCurrentUser();

            Answer answer = null;
            var mainQuery = Session.QueryOver<Answer>(() => answer);

            mainQuery = mainQuery.OrderBy(p => p.CreationDate).Desc;

            if (PostId.HasValue)
                mainQuery = mainQuery.Where(c => c.Post.Id == PostId);
            
            UserProfile answerAuthor = null;
            mainQuery = mainQuery.JoinAlias(() => answer.Author, () => answerAuthor);

            return WithPaging(mainQuery.SelectList(
                list => list
                            .Select(c => c.Author.Id)
                            .Select(c => c.Id)
                            .Select(c => answerAuthor.Username)
                            .Select(c => answerAuthor.Provider)
                            .Select(c => answerAuthor.ProviderUserId)
                )).List<object[]>().Select(l => new
                {
                    Author = l[0],
                    Id = l[1],
                    Username = l[2],
                    ProfileImageLink = new ProfileImage((string)l[3], (string)l[4]).Url,
                    ProfileLink = new ProfileLink((string)l[3], (string)l[4]).Url
                });
        }

        private void ValidatePostForCurrentUser()
        {
            var post = Session.Load<Post>(PostId);

            if (post.Author.Id != CurrentUser.Id && !CurrentUser.IsAdmin)
                throw new InvalidOperationException("Post not for current user.");
        }
    }
}