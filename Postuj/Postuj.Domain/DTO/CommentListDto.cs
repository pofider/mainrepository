﻿using System;

namespace Postuj.Domain.DTO
{
    public class CommentListDto
    {
        public string Content { get; set; }
        public long Author { get; set; }
        public DateTime CreationDate { get; set; }
        public long Id { get; set; }
        public string CreationDateString { get; set; }
        public string Username { get; set; }
        public string ProfileImageLink { get; set; }
        public string ProfileLink { get; set; }
    }
}