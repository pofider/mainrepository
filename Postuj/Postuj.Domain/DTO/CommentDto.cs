﻿namespace Postuj.Domain.DTO
{
    public class CommentDto
    {
        public string Content { get; set; }
        public long PostId { get; set; }
    }
}