﻿using System;
using System.Collections.Generic;
using Postuj.Domain.Domain;

namespace Postuj.Domain.DTO
{
    public class PostListDto
    {
        public string Content { get; set; }
        public long Author { get; set; }
        public int Likes { get; set; }
        public int Comments { get; set; }
        public int Dislikes { get; set; }
        public LocationEnum Location { get; set; }
        public string CategoryCode { get; set; }
        public DateTime CreationDate { get; set; }
        public long Id { get; set; }
        public string LocationString { get; set; }
        public string CategoryString { get; set; }
        public string CreationDateString { get; set; }
        public string Username { get; set; }
        public string ProfileImageLink { get; set; }
        public string ProfileLink { get; set; }
        public string AttachedImageLink { get; set; }
        public string AttechedImageThumbLink { get; set; }
        public string VideoLink { get; set; }
        public string VideoThumb { get; set; }
        public int Answers { get; set; }
        public bool HasMyAnswer { get; set; }
        public IEnumerable<CommentListDto> CommentsList { get; set; }

        public string AttachedImageThumbLink { get; set; }
    }
}