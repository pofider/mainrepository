﻿namespace Postuj.Domain.DTO
{
    public class PostDto
    {
        public string Content { get; set; }
        public string CategoryCode { get; set; }
        public string LocationCode { get; set; }
        public string VideoLink { get; set; }
        public string VideoThumb { get; set; }
    }
}