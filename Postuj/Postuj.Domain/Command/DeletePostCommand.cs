﻿using NHibernate.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public class DeletePostCommand : GenericCommandBase<bool>
    {
        public long Id { get; set; }

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var post = Session.Load<Post>(Id);

            if (post.Author != (UserProfile) CurrentUser && !(CurrentUser).IsAdmin)
            {
                Fail("Neni povoleno smazat cizí příspěvek.");
                return;
            }

            Session.Delete(post);

            Cache.Clear();
            Success(true);
        }
    }
}