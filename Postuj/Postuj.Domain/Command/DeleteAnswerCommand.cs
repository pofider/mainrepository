﻿using System.Linq;
using NHibernate.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public class DeleteAnswerCommand : GenericCommandBase<bool>
    {
        public long PostId { get; set; }

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var post = Session.Load<Post>(PostId);

            var answer = Session.Query<Answer>().Where(a => a.Post.Id == post.Id && a.Author.Id == CurrentUser.Id).Single();

            post.RemoveAnswer(answer);

            Session.SaveOrUpdate(post);

            Cache.Clear();

            Success(true);
        }
    }
}