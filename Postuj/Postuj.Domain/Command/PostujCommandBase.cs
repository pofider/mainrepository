﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public abstract class PostujCommandBase : CommandBase
    {
        public new UserProfile CurrentUser
        {
            get { return base.CurrentUser as UserProfile; }
            set { base.CurrentUser = value; }
        }
    }

    public abstract class PostujGenericCommandBase<TResult> : GenericCommandBase<TResult>
    {
        public new UserProfile CurrentUser
        {
            get { return base.CurrentUser as UserProfile; }
            set { base.CurrentUser = value; }
        }
    }
}