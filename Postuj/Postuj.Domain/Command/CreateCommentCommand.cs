﻿using System;
using System.Web;
using BitlyDotNET.Interfaces;
using Postuj.Domain.Builders;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;

namespace Postuj.Domain.Command
{
    public class CreateCommentCommand : PostujGenericCommandBase<Comment>
    {
        public CommentDto CommentDto { get; set; }
        public IBitlyService BitlyService { get; set; }

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var post = Session.Load<Post>(CommentDto.PostId);


            CommentDto.Content = HttpUtility.HtmlEncode(CommentDto.Content);
            CommentDto.Content = CommentDto.Content.FormatUrls();
            CommentDto.Content = CommentDto.Content.ShortenUrls(BitlyService);

            var comment = new CommentBuilder(false)
                            .WithPost(post)
                            .WithAuthor(CurrentUser)
                            .WithContent(CommentDto.Content)
                            .WithCreationDate(DateTime.Now)
                            .Build();

            post.AddComment(comment);
            Session.SaveOrUpdate(post);
            Session.Flush();

            Cache.Clear();

            Success(comment);
        }
    }
}