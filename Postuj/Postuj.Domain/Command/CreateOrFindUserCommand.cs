﻿using NHibernate.Linq;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;
using System.Linq;

namespace Postuj.Domain.Command
{
    public class CreateOrFindUserCommand : GenericCommandBase<UserProfile>
    {
        public string ProviderUserId { get; set; }
        public string Provider { get; set; }
        public string Username { get; set; }

        protected override void ExecuteInternal()
        {
            var userProfile = Session.Query<UserProfile>()
                                        .Where(u => u.Provider == Provider)
                                        .Where(u => u.ProviderUserId == ProviderUserId)
                                        .SingleOrDefault();

            if (userProfile != null)
            {
                Success(userProfile);
                return;
            }

            userProfile = new UserProfileBuilder(false)
                .WithProvider(Provider)
                .WithProviderUserId(ProviderUserId)
                .WithUsername(Username)
                .BuildAndWriteToDatabase(Session);

            Success(userProfile);
        }
    }
}