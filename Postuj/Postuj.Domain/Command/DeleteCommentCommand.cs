﻿using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public class DeleteCommentCommand : GenericCommandBase<bool>
    {
        public long CommentId { get; set; } 

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var comment = Session.Load<Comment>(CommentId);

            if (comment.Author.Id != CurrentUser.Id && !CurrentUser.IsAdmin)
            {
                Fail("Komentář nepatří přihlášenému uživateli.");
                return;
            }

            var post = comment.Post;

            post.RemoveComment(comment);
            Session.SaveOrUpdate(comment);

            Cache.Clear();
        }
    }
}