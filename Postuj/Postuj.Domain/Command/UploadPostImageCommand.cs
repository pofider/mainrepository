﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.FileSystem;

namespace Postuj.Domain.Command
{
    public class UploadPostImageCommand : GenericCommandBase<bool>
    {
        public Stream InputStream { get; set; }
        public long PostId { get; set; }
        public IStorageProvider StorageProvider { get; set;}

        protected override void ExecuteInternal()
        {
            var parser = new MultipartParser(InputStream, ensureParsed: true);

            foreach (var content in parser.MyContents)
            {
                var imageReference = new FileReferenceBuilder()
                                        .WithFileName(Path.GetFileName(content.PropertyName))
                                        .WithContentType(parser.ContentType)
                                        .WithUniqueId(PostId.ToString(CultureInfo.InvariantCulture))
                                        .Build();

                var thumbRefernce = new FileReferenceBuilder()
                                        .WithFileName(Path.GetFileName(content.PropertyName))
                                        .WithContentType(parser.ContentType)
                                        .WithUniqueId("T" + PostId)
                                        .Build();

                var file = StorageProvider.CreateOrReplaceFile(imageReference.PrivateUri);
                
                using (var stream = file.OpenWrite())
                {
                    stream.Write(content.Data, 0, content.Data.Length);
                    using (Image thumbnailImage = Image.FromStream(new MemoryStream(content.Data)))
                    {
                        using (Image thumbImg = CreateThumbnail(thumbnailImage, 490, 275, true))
                        {
                            var thumb = StorageProvider.CreateOrReplaceFile(thumbRefernce.PrivateUri);

                            using (var thumbBlobStream = thumb.OpenWrite())
                            {
                                thumbImg.Save(thumbBlobStream, ImageFormat.Png);
                            }
                        }
                    }
                }

                var post = Session.Load<Post>(PostId);
                post.IsImageUploaded = true;
                post.ImageReference = imageReference;
                post.ThumbImageReference = thumbRefernce;

                Session.SaveOrUpdate(post);
            }

            Success(true);
        }

        protected Image CreateThumbnail(Image originalImage, int thumbnailWidth, int thumbnailHeight, bool keepAspectRatio)
        {
            int newWidth; int newHeight;
            GetThumbnailSize(originalImage, thumbnailWidth, thumbnailHeight, keepAspectRatio, out newWidth, out newHeight);

            Bitmap newImage = new Bitmap(originalImage, newWidth, newHeight);

            Graphics g = Graphics.FromImage(newImage);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;

            g.DrawImage(originalImage, 0, 0, newImage.Width, newImage.Height);

            return newImage;
        }

        protected void GetThumbnailSize(Image originalImage, int thumbnailWidth, int thumbnailHeight, bool keepAspectRatio, out int newWidth, out int newHeight)
        {
            newWidth = thumbnailWidth;
            newHeight = thumbnailHeight;

            if (keepAspectRatio)
            {
                if (originalImage.Width > originalImage.Height)
                {
                    newWidth = thumbnailWidth;
                    float widthPer = (float)thumbnailWidth / originalImage.Width;
                    newHeight = Convert.ToInt32(originalImage.Height * widthPer);
                }
                else
                {
                    newHeight = thumbnailHeight;
                    float heightPer = (float)thumbnailHeight / originalImage.Height;
                    newWidth = Convert.ToInt32(originalImage.Width * heightPer);
                }
            }
        }
    }
}