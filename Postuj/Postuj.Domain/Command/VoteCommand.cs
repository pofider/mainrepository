﻿using System.Linq;
using NHibernate.Linq;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public class VoteCommand : GenericCommandBase<bool>
    {
        public long Id { get; set; }
        public bool IsDislike { get; set; }

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var anyVote = Session.Query<Vote>()
                            .Where(v => v.Post.Id == Id)
                            .Where(v => v.Author.Id == CurrentUser.Id)
                            .Any();

            if (anyVote)
            {
                Success(false);
                return;
            }

            var post = Session.Load<Post>(Id);
            post.AddVote(new VoteBuilder(false).WithIsLike(!IsDislike).WithAuthor((UserProfile)CurrentUser).Build());
            
            Session.SaveOrUpdate(post);
            
            Cache.Clear();
            Success(true);
        }
    }
}