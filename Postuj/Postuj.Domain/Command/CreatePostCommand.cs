﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using BitlyDotNET.Interfaces;
using Postuj.Domain.Builders;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;

namespace Postuj.Domain.Command
{
    public class CreatePostCommand : GenericCommandBase<Post>
    {
        public PostDto PostDto { get; set; }
        public IBitlyService BitlyService { get; set; }

        protected override void ExecuteInternal()
        {
            var video = ReplaceVideoLink(PostDto);

            PostDto.Content = HttpUtility.HtmlEncode(PostDto.Content);
            PostDto.Content = PostDto.Content.Replace("\n", "<br>");
            PostDto.Content = PostDto.Content.FormatUrls();
            PostDto.Content = PostDto.Content.ShortenUrls(BitlyService);

            var post = new PostBuilder(false)
                        .WithContent(PostDto.Content)
                        .WithCategory(PostDto.CategoryCode.ToCategoryEnum().Value)
                        .WithCreationDate(DateTime.Now)
                        .WithLocation(PostDto.LocationCode.ToLocationEnum().Value)
                        .WithVideoLink(video.IFrame)
                        .WithVideoThumb(video.Thumb)
                        .WithVideoThumbLink(video.ThumbLink)
                        .WithFacebookVideoLink(video.Facebook)
                        .WithOriginalVideoLink(video.Original)
                        .WithAuthor((UserProfile)CurrentUser)
                        .BuildAndWriteToDatabase(Session);

            Session.Flush();
            Cache.Clear();
            
            Success(post);
        }

        private Video ReplaceVideoLink(PostDto post)
        {
            //http://www.youtube.com/v/TYYW_WwYHuM?version=3&autohide=1

            string output = post.Content;
            Regex regx = new Regex(@".*(https?:\/\/www.youtube.com\/watch\?v=([a-zA-Z0-9_\-]+)[^\s]*).*", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(output);

            string videoLink = null;
            string videoThumb = null;
            string original = null;
            string facebookVideoLink = null;
            string thumbLink = null;

            foreach (Match match in mactches)
            {
                original = match.Groups[1] + "";
                output = output.Replace(match.Groups[1] + "", "").TrimStart();

                var videoId = match.Groups[2];

                facebookVideoLink = "https://youtube.com/v/" + videoId + "?version=3&autohide=1";

                videoLink = "<iframe width='490' class='img-polaroid rounded-corners' id='youtube' height='275' src='http://www.youtube.com/embed/"
                   + videoId + "?wmode=opaque&autoplay=1' frameborder='0' allowfullscreen></iframe>";

                thumbLink = "http://img.youtube.com/vi/" + videoId + "/0.jpg";
                videoThumb = "<div class='youtube-thumb-box'><img src='" + thumbLink + "' class='youtube-thumb'></img></div>";
         
                if (post.CategoryCode == "news")
                    post.CategoryCode = "videos";
            }

            post.Content = output;

            return new Video() { IFrame = videoLink, Thumb = videoThumb, Original = original, Facebook = facebookVideoLink, ThumbLink = thumbLink};
        }

        private class Video
        {
            public string IFrame { get; set; }
            public string Thumb { get; set; }
            public string ThumbLink { get; set; }
            public string Original { get; set; }
            public string Facebook { get; set; }
        }
    }
}