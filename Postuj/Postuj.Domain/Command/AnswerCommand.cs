﻿using System.Linq;
using NHibernate.Linq;
using Postuj.Domain.Builders;
using Postuj.Domain.Domain;
using Postuj.Infrastructure.Command;

namespace Postuj.Domain.Command
{
    public class AnswerCommand : GenericCommandBase<bool>
    {
        public long Id { get; set; }

        protected override void ExecuteInternal()
        {
            Cache.Clear();

            var anyAnswer = Session.Query<Answer>()
                            .Where(v => v.Post.Id == Id)
                            .Where(v => v.Author.Id == CurrentUser.Id)
                            .Any();

            if (anyAnswer)
            {
                Success(false);
                return;
            }

            var post = Session.Load<Post>(Id);
            post.AddAnswer(new AnswerBuilder(false).WithAuthor((UserProfile)CurrentUser).Build());
            Session.SaveOrUpdate(post);

            Cache.Clear();

            Success(true);
        }
    }
}