﻿using System.Collections.Generic;
using System.Web.Http;
using NHibernate;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Query;

namespace Postuj.Web.WebServices
{
    public class ApiControllerBase : ApiController 
    {
        protected ISession Session { get; private set; }
        protected IQueryExecutor QueryExecutor { get; private set; }
        public ICommandExecutor CommandExecutor { get; private set; }

        public ApiControllerBase(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
        {
            Session = session;
            QueryExecutor = queryExecutor;
            CommandExecutor = commandExecutor;
        }

        protected ApiControllerBase()
        {
            
        }

        protected IEnumerable<TResult> Query<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.Execute(query);
        }

        protected TResult QuerySingle<TResult>(IQuery<TResult> query)
        {
            return QueryExecutor.ExecuteSingle(query);
        }

        protected TResult Command<TResult>(IGenericCommand<TResult> command)
        {
            return CommandExecutor.Execute(command).Result;
        }

        protected void Command(ICommand command)
        {
            CommandExecutor.Execute(command);
        }

    }
}