using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Http;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using NHibernate;
using Postuj.Domain.Command;
using Postuj.Domain.DTO;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Web.WebServices
{
    public class Filter
    {
        public string CategoryCode { get; set; }
        public string LocationCode { get; set; }
        public string PostOrderCode { get; set; }
        public bool? AnsweredByMe { get; set; }
        public string SearchString { get; set; }
        public bool? PostedByMe { get; set; }
        public long? PostId { get; set; }
        public int? PageNumber { get; set; }
    }

    public class PostController : ApiControllerBase
    {
        // GET api/<controller>
        public PostController(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor) : base(session, queryExecutor, commandExecutor)
        {
        }

        //[GET("api/Post?&tagged={category}&from={location}&page={pageNumber}&postedByMe={postedByMe}&answeredByMe={answeredByMe}&postId={postId}&orderBy={orderBy}&searchString={searchString}")]
        //[GET("api/Post/Tagged/{category}/From/{location}/Page/{pageNumber}/PostedByMe/{postedByMe}/AnsweredByMe/{answeredByMe}/PostId/{postId}/OrderBy/{orderBy}/SearchString/{searchString}")]
        //public IEnumerable<object> Get(string category, string location, int? pageNumber, bool? postedByMe, bool? answeredByMe, long? postId, string orderBy, string searchString)
        //{
        //    return Query(new PostListQuery() { 
        //        CategoryCode = category, LocationCode = location, PageNumber = pageNumber, 
        //        PostedByMe = postedByMe, AnsweredByMe = answeredByMe, PostId = postId,
        //        PostOrderCode = orderBy, SearchString = searchString});
        //}


        [POST("api/Post/Filter")]
        public IEnumerable<object> PostFilter([FromBody] Filter filter)
        {
            return Query(new PostListQuery()
            {
                CategoryCode = filter.CategoryCode,
                LocationCode = filter.LocationCode,
                PageNumber = filter.PageNumber,
                PostedByMe = filter.PostedByMe,
                AnsweredByMe = filter.AnsweredByMe,
                PostId = filter.PostId,
                PostOrderCode = filter.PostOrderCode,
                SearchString = filter.SearchString
            });
        }

        [GET("api/Test/Path")]
        public string GetPath()
        {
            return Microsoft.WindowsAzure.ServiceRuntime.RoleEnvironment.GetLocalResource("Log4Net").RootPath;
        }
      

        [GET("api/Post/{postId}/Answers")]
        public IEnumerable<object> Get(long postId)
        {
            return Query(new AnswerListQuery() { PostId = postId});
        }

        [POST("api/Post/{id}/Like")]
        public bool Like(int id)
        {
            return Command(new VoteCommand() {Id = id});
        }

        [POST("api/Post/{id}/Dislike")]
        public bool Dislike(int id)
        {
            return Command(new VoteCommand() { Id = id, IsDislike = true });
        }

        [POST("api/Post/{id}/Answer")]
        public bool Answer(int id)
        {
            return Command(new AnswerCommand() { Id = id });
        }


        [DELETE("api/Post/{id}/Answer")]
        public bool DeleteAnswer(int id)
        {
            return Command(new DeleteAnswerCommand() { PostId = id });
        }

        [GET("api/Post/{id}/Comment")]
        public IEnumerable<object> GetComments(long id)
        {
            return Query(new CommentListQuery() { PostId = id});
        }

        [POST("api/Post/{id}/Comment")]
        public object CreateComment([FromBody]CommentDto comment, int id)
        {
            comment.PostId = id;
            var newComment = Command(new CreateCommentCommand() {CommentDto = comment});
            return Query(new CommentListQuery() { CommentId = newComment.Id }).Single();
        }

        [POST("api/Post/{id}/Image")]
        public HttpResponseMessage  UploadImage(long id)
        {
            Command(new UploadPostImageCommand()
                        {
                            InputStream = HttpContext.Current.Request.InputStream,
                            PostId = id
                        });

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent("ok");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

            return result;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        [POST("api/Post")]
        public object Post([FromBody]PostDto post)
        {
            return Query(new PostListQuery()
                {
                    PostId = Command(new CreatePostCommand() {PostDto = post}).Id,
                    SupressUnfinishedImagesFiltering = true
                }).Single();
        }
        
        [DELETE("api/Post/{id}")]
        public void Delete(int id)
        {
            Command(new DeletePostCommand() { Id = id });
        }

        [DELETE("api/Post/{id}/Comment/{commentId}")]
        public void Delete(int id, int commentId)
        {
            Command(new DeleteCommentCommand() { CommentId = commentId });
        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }
    }
}

public class TextFormatter : MediaTypeFormatter
{
    public override bool CanReadType(Type type)
    {
        return true;
    }

    public override bool CanWriteType(Type type)
    {
        return true;
    }
}