﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Autofac;
using Postuj.Web.Controllers;

namespace Postuj.Web.WebServices
{
    public class WebServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PostController>();
            builder.RegisterType<UserCardController>();
            builder.RegisterType<ImageController>();
        }
    }
}