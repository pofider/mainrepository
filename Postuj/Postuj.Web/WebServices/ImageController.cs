﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using AttributeRouting.Web.Http;
using NHibernate;
using Postuj.Domain.Domain;
using Postuj.Domain.Query;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.FileSystem;
using Postuj.Infrastructure.Query;

namespace Postuj.Web.WebServices
{
    public class ImageController: ApiControllerBase
    {
        private readonly IStorageProvider _storageProvider;

        // GET api/<controller>
        public ImageController(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor,
            IStorageProvider storageProvider) : base(session, queryExecutor, commandExecutor)
        {
            _storageProvider = storageProvider;
        }

        [GET("api/Image/{uniqueKey}")]
        public HttpResponseMessage Get(string uniqueKey)
        {
            FileReference fileRerencence;

            if (uniqueKey.Contains("T"))
                fileRerencence = Session.Load<Post>(long.Parse(uniqueKey.Remove(0, 1))).ThumbImageReference;
            
            else
                fileRerencence = Session.Load<Post>(long.Parse(uniqueKey)).ImageReference;

            if (!_storageProvider.FileExists(fileRerencence.PrivateUri))
                return null;

            var file = _storageProvider.GetFile(fileRerencence.PrivateUri);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(file.OpenRead());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue(fileRerencence.ContentType);
            return result;
        }
    }
}