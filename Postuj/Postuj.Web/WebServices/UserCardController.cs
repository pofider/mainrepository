﻿using System.Collections.Generic;
using AttributeRouting.Web.Http;
using NHibernate;
using Postuj.Domain.Query;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Query;

namespace Postuj.Web.WebServices
{
    public class UserCardController: ApiControllerBase
    {
        public UserCardController(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
            : base(session, queryExecutor, commandExecutor)
        {
        }

        [GET("api/UserCard/New/Notifications")]
        public IEnumerable<object> GetNewNotifications()
        {
            return Query(new NewNotificationCompositeQuery());
        }

        [GET("api/UserCard/Old/Notifications")]
        public IEnumerable<object> GetOldNotifications()
        {
            return Query(new OldNotificationCompositeQuery());
        }

        [GET("api/UserCard/Notifications/Page/{pageNumber}")]
        public object GetNotifications(int? pageNumber)
        {
            return QuerySingle(new NotificationsCompositeQuery() { PageNumber = pageNumber});
        }

        [GET("api/UserCard/Count")]
        public object GetCounts()
        {
            return QuerySingle(new UserCardQuery());
        }
    }
}