﻿using Autofac;

namespace Postuj.Web.Controllers
{
    public class ControllersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AccountController>();
            builder.RegisterType<HomeController>();
        }
    }
}