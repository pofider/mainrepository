﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using Postuj.Domain.Domain;
using Postuj.Domain.Shared;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;

namespace Postuj.Web.Controllers
{
    public class HomeController : PostujControllerBase
    {
        public HomeController(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor)
            : base(session, queryExecutor, commandExecutor)
        {
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult List(long id)
        {
            if (Request.Headers["user-agent"].StartsWith("facebook") || Request.QueryString["facebook"] == "true")
            {
                var post = Session.Load<Post>(id);

                string imageLink = "http://postuj.cz/Images/fb-thumb.png";
                string imageContentType = "image/png";

                if (post.ThumbImageReference != null && post.ThumbImageReference.PublicUri != null)
                {
                    imageLink = "http://postuj.cz/" + post.ThumbImageReference.PublicUri;
                    imageContentType = post.ThumbImageReference.ContentType;
                }

                if (post.Category == CategoryEnum.Video)
                {
                    imageLink = post.VideoThumbLink;
                }

                return View("OgMeta", new OgModel
                    {
                        Title = string.IsNullOrEmpty(post.Content) ? "Postuj.cz" : 
                        (post.Content.Substring(0, post.Content.Length > 23 ? 23 : post.Content.Length) + (post.Content.Length > 23 ? " ..." : "")),
                        Description = post.Content,
                        Image = imageLink,
                        ImageContentType = imageContentType,
                        Url = "http://postuj.cz/List/" + id
                    });
            }

            //return Redirect("http://localhost:30320/#/list?PostId=" + id);
            return Redirect("http://postuj.cz/#/list?PostId=" + id);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Templates()
        {
            var templates =
                Directory.EnumerateFiles(Server.MapPath("~/Templates/"), "*.tmpl.html", SearchOption.AllDirectories)
                         .Select(t =>
                                 new
                                     {
                                         templateName = new FileInfo(t).Name.Replace(".tmpl.html", ""),
                                         content = GetContent(t)
                                     });

            return Json(templates, JsonRequestBehavior.AllowGet);
        }

        private string GetContent(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                return reader.ReadToEnd();
            }
        }
    }

    public class OgModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string ImageContentType { get; set; }
        public string VideoLink { get; set; }
        public string Url { get; set; }
    }
}