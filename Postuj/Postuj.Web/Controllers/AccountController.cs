﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using NHibernate;
using Postuj.Domain.Command;
using Postuj.Infrastructure.Command;
using Postuj.Infrastructure.Common;
using Postuj.Infrastructure.Query;
using WebMatrix.WebData;
using Postuj.Web.Models;

namespace Postuj.Web.Controllers
{
    [Authorize]
    public class AccountController : PostujControllerBase
    {
        public AccountController(ISession session, IQueryExecutor queryExecutor, ICommandExecutor commandExecutor) : base(session, queryExecutor, commandExecutor)
        {
        }

        //
        // GET: /Account/L
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddYears(-1);
            Session.Clear();

            FormsAuthentication.SignOut();
            
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }
        
        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            if (UseTestAcount())
                return ExternalLoginCallback(returnUrl);

            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        private bool UseTestAcount()
        {
            var settings = ConfigurationManager.AppSettings["UseTestingAccount"];
            
            bool useSettings;
            if (!bool.TryParse(settings, out useSettings))
                return false;

            return useSettings;
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result;

            if (!UseTestAcount())
            {
                result =
                    OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback",
                                                                     new {ReturnUrl = returnUrl}));
                if (!result.IsSuccessful)
                {
                    return RedirectToAction("ExternalLoginFailure");
                }
            }
            else
            {
                result = new AuthenticationResult(true, "facebook", "1", "Jan Blaha", new Dictionary<string, string>());
            }

            var userProfile = Command(new CreateOrFindUserCommand()
                        {
                            Username = result.ExtraData.ContainsKey("name") && !string.IsNullOrEmpty(result.ExtraData["name"]) ? 
                            result.ExtraData["name"] : result.UserName
,
                            Provider = result.Provider,
                            ProviderUserId = result.ProviderUserId
                        });

            userProfile.IsAuthenticated = true;
            HttpContext.User = userProfile;
            FormsAuthentication.SetAuthCookie(userProfile.Id.ToString(), true);


            return RedirectToLocal(returnUrl);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }
      

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }
      
        #endregion
    }
}
