﻿  
    $.ajaxSetup({
        error: function (jqXHR, textStatus, errorThrown) {
            $(".loader").hide();
            $.dialog({
                header: "Nastala chyba",
                content: errorThrown,
                error: true
            });
        }
    });

    $.fn.postData = function (path, data, success) {
        $.ajax({
            type: "POST",
            url: path,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            success: function (result) {
                success(result);
            },
        });
    };

    $.fn.putData = function (path, data, success) {
        $.ajax({
            type: "PUT",
            url: path,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            success: function (result) {
                success(result);
            },
        });
    };

    $.fn.deleteData = function (path, data, success) {
        $.ajax({
            type: "DELETE",
            url: path,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            success: function (result) {
                success(result);
            },
         });
    };

    $.fn.dropdowns = function () {
        $('.dropdown-toggle').dropdown();
        $(".dropdown-toggle").next().find("li a").click(function () {
            var btn = $(this).parent().parent().prev('.dropdown-toggle').prev();
            btn.text($(this).text());
            btn.val($(this).attr("data-value"));
            btn.trigger("change");
        });
    };


    $.fn.removeElement = function (list, equealsHandler) {
        for (var i = 0; i < list.length; i++) {
            if (equealsHandler(list[i])) {
                list.splice(i, 1);
                return;
            }
        }
    };

    $.fn.findId = function() {
        return this.closest("[dataId]").attr("dataId");
    };

    $.getUrlVars = function() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    };

    $.extend({
        putData: $.fn.putData,
        postData: $.fn.postData,
        deleteData: $.fn.deleteData,
        removeElement: $.fn.removeElement,
        dropdowns: $.fn.dropdowns,
        getUrlVars: $.fn.getUrlVars,
    });

    $.fn.extend({
        findId: $.fn.findId,
    });



Date.prototype.addHours = function (h) {
    var copiedDate = new Date(this.getTime());
    copiedDate.setHours(copiedDate.getHours() + h);
    return copiedDate;
}