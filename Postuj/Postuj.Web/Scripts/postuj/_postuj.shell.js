﻿var postuj = postuj || {};

$.ajaxSetup({ cache: false });

$(function () {
    
    if (window.location.hash == "#_=_") { //facebook is redirecting to wierd url, lets redirect back
        window.location.href = window.location.href.substr(0, window.location.href.indexOf('#'));
    }
    
    //load and compile templates
    $.getJSON("Home/Templates", null, function (controllerData) {
        window.templates = controllerData;

        for (var i = 0; i < window.templates.length; i++) {
            $.templates(window.templates[i].templateName, window.templates[i].content);
        };

        postuj.user = {};
        postuj.user.id = $("#currentUser").val();
        postuj.user.isAdmin = $("#currentUserIsAdmin").val() == "True";
        postuj.user.profileImage = $("#currentUserProfileImage").val();

        postuj.initialized = true;
        $(postuj).trigger("shell-loaded");
        
        if (postuj.user.id != null) {
            $(postuj).trigger("user-authenticated");
        }

        $.views.helpers({
            user : function() {
                return postuj.user;
            }
        });

        $.dropdowns();

    });
    
    $(".search-query").bind("keypress", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
            
        if (code == 13) {
            postuj.list.setSearchString($(this).val());

            if (postuj.list.SearchString == "")
                postuj.list.SearchString = null;

            postuj.list.load();
            return false;
        }
    });

});