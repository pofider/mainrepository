﻿var postuj = postuj || {};

postuj.menuWidget = {
   
    root: function () {
        return $(".main-menu");
    },
    
    initHandlers: function () {
        var self = this;
   
        self.root().find("#menu li a.text").click(function () {
            postuj.list.setCategory("news");
            self.activate("text");
            postuj.list.load();
        });

        self.root().find("#menu li a.home").click(function () {
            postuj.list.setCategory("all");
            self.activate("home");
            postuj.list.load();
        });

        self.root().find("#menu li a.media").click(function () {
            postuj.list.setCategory("media");
            self.activate("media");
            postuj.list.load();
        });
        
        self.root().find("#menu li a.video").click(function () {
            postuj.list.setCategory("videos");
            self.activate("video");
            postuj.list.load();
        });

        self.root().find("#menu li a.event").click(function () {
            postuj.list.setCategory("events");
            self.activate("event");
            postuj.list.load();
        });

        self.root().find("#menu li a.love").click(function () {
            postuj.list.setCategory("meetings");
            self.activate("love");
            postuj.list.load();
        });

        self.root().find(".btn-location").change(function() {
            postuj.list.setLocation(self.root().find(".btn-location").val());
            postuj.list.load();
        });

    },
    
    menuLinks: function() {
        return this.root().find("li a");
    },
    
    activate: function (cssClass) {
        this.menuLinks().removeClass("active");
        this.root().find("li a." + cssClass).addClass("active");
    },
    
    activateMenu: function() {
        switch(postuj.list.Category) {
            case "news":
                this.activate("text");break;
            case "media":
                this.activate("media");break;
            case "videos":
                this.activate("video");break;
            case "events":
                this.activate("event");break;
            case "meetings":
                this.activate("love");break;
            default:
                this.activate("home");
        }
    }
};

$(postuj).bind("shell-loaded", function () {
    postuj.menuWidget.initHandlers();
});

$(postuj).bind("posts-loaded", function () {
    postuj.menuWidget.activateMenu();
});