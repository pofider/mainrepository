﻿var postuj = postuj || {};

postuj.newWidget = {
    Category: "news",
    UploadedImageId: null,
    Uploader: null,
    SubmitedFile: null,
    CurrentPost: null,

    root: function() {
        return $(".insert-post");
    },
    
    createPost: function () {
        $(".loader").show();
        var self = this;
            
        var post = new postuj.post();
        
        post.LocationCode = self.root().find(".btn-choose-loc").val();
        post.Content = self.root().find("#newPostTextArea").val();
        
        if (self.SubmitedFile && self.Category == "news")
            post.CategoryCode = "media";
        else 
            post.CategoryCode = self.Category;
       
        post.create({
            success: function (p) {
                self.PostId = p.Id;
                self.CurrentPost = post;
                
                self.root().find("#newPostTextArea").val(null);
                self.countChar(self.root().find("#newPostTextArea"));

                if (self.SubmitedFile) {
                    self.Uploader.fineUploader('uploadStoredFiles');
                } else {
                    postuj.list.loadForNew(p);
                }
                postuj.userCardWidget.reload();
            }
        });
    },
    
    newsCategory: function () {
        this.categoryLinks().removeClass("active");
        this.root().find("a.item1").addClass("active");
        this.Category = "news";
    },

    eventsCategory: function () {
        this.categoryLinks().removeClass("active");
        this.root().find("a.item2").addClass("active");
        this.Category = "events";
    },
    
    meetingsCategory: function () {
        this.categoryLinks().removeClass("active");
        this.root().find("a.item3").addClass("active");
        this.Category = "meetings";
    },
    
    countChar: function countChar(input) {
        var len = input.val().length;

        if (input.val().replace(/\s+/g, '').length == 0 && this.SubmitedFile == null)
            this.root().find("#newPostBtn").attr("disabled", "disabled");
        else
            this.root().find("#newPostBtn").removeAttr("disabled");
        
        if (len >= 400) {
            input.val(input.val().substring(0, 400));
            this.root().find('.letters-no').text(0);
        }
        else {
            this.root().find('.letters-no').text(400 - len);
        }
    },

    
    initHandlers: function () {
        var self = this;
        
        this.root().find("#newPostBtn").click(function () {
            self.createPost();
            return false;
        });

        this.root().find("#newPostTextArea").keyup(function() {
            self.countChar($(this));
        });
        
        this.root().find("#newPostTextArea").bind("input",function () {
            self.countChar($(this));
        });

        this.countChar(this.root().find("#newPostTextArea"));
        
        this.root().find(".item1").click(function () {
            self.newsCategory();
        });
        
        this.root().find(".item2").click(function () {
            self.eventsCategory();
        });
        
        this.root().find(".item3").click(function () {
            self.meetingsCategory();
        });

        this.root().find("#uploadButtn").click(function () {
            self.root().find("#uploadModal").modal();
        });

        self.Uploader = new $(self.root().find('#fine-uploader')).fineUploader({
                element: self.root().find('#fine-uploader'),
                request: {
                    endpoint: function() { return 'api/Post/' + self.PostId + '/Image'; },
                },
                text: {
                    uploadButton: '<i class="icon-camera icon-white"></i> Obrázek'
                },
                template: '<div>' +
                    '<pre class="qq-upload-drop-area"><span>{dragZoneText}</span></pre>' +
                    '<div class="qq-upload-button btn btn-upload-img btn-small btn-success" id="uploadButtn" style="display:inline">{uploadButtonText}</div>' +
                    '<ul class="qq-upload-list"></ul>' +
					'<a class="close" id="removeFile" style="display: none;font-size: 16px" title="Odstranit obrázek">×</a>' +
                    '</div>',
                classes: { success: 'alert upload-success', fail: 'alert upload-error' },
                debug: true,
                multiple: false,
                forceMultipart: true,
                autoUpload: false,
                validation: {
                    allowedExtensions: ['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'gif'],
                    acceptFiles: 'image/*',
                    sizeLimit: 2097152,
                },

            }
        );
        
       
        self.Uploader.on("complete", function () {
            postuj.list.loadForNew(self.CurrentPost);
            self.root().find("#newPostTextArea").val(null);
            self.root().find("#removeFile").hide();
            self.SubmitedFile = null;
            self.Uploader.fineUploader('clearStoredFiles');
        });
        
        self.Uploader.on("submit", function () {
            self.root().find("#removeFile").show();
            self.SubmitedFile = true;
            
            self.countChar(self.root().find("#newPostTextArea"));
            
            self.root().find("#removeFile").unbind("click").click(function () {
                self.Uploader.fineUploader('clearStoredFiles');
                self.SubmitedFile = null;
                self.root().find("#removeFile").hide();
            });
        });
    },
    
    categoryLinks: function() {
        return this.root().find("#categ li a");
    }
};

$(postuj).bind("user-authenticated", function () {
    postuj.newWidget.initHandlers();
});