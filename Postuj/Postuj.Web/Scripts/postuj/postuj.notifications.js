﻿var postuj = postuj || {};

postuj.notifications = {
    PageNumber: 1,
    CurrentNotifications: null,
    
    root: function () {
        return $(".posted");
    },
    
    getData: function(callback) {
        $.getJSON("api/UserCard/Notifications" + "/Page/" + this.PageNumber, function (data) {
            callback(data);
        });
    },
    
    bindNotificationHandlers: function() {
        this.root().find(".post-link").click(function () {
            postuj.list.reset();
            postuj.list.PostId = $(this).attr("data-id");
            postuj.list.load();
        });
    },

    load: function () {
        var self = this;

        self.root().html($.render["notifications-area"]);

        self.PageNumber = 1;
        
        self.getData(function(data) {
            self.root().find("#old-notifications").html($.render["notification"](data.Old));
            self.root().find("#new-notifications").html($.render["notification"](data.New));

            self.bindNotificationHandlers();
        });
       

        self.root().find("#loadAnotherPage").click(function() {
            self.PageNumber += 1;
            self.getData(function (data) {
                self.root().find("#old-notifications").append($.render["notification"](data.Old));
                self.bindNotificationHandlers();
            });
        });
    }
}