﻿var postuj = postuj || {};

postuj.userCardWidget = {
    reloadStarted: false,

    root: function () {
        return $("#userCard");
    },

    init: function () {
        _.bindAll(this, "reload");
        
        var self = this;

        this.root().find("#show-notifications-command").click(function () {
            postuj.notifications.load();
        });
        
        this.root().find(".profil").click(function () {
            window.open($(this).attr("data-link"), '_blank');
            window.focus();
        });
       
        
        this.root().find("#show-answers-command").click(function () {
            postuj.list.reset();
            postuj.list.AnsweredByMe = true;
            postuj.list.load();
        });
        
        this.root().find("#show-posts-command").click(function () {
            postuj.list.reset();
            postuj.list.PostedByMe = true;
            postuj.list.load();
        });

        if (this.reloadStarted == false && postuj.user.id != null) {
            this.reloadStarted = true;
            this.reload();
            window.setInterval(this.reload, 10000);
        }
    },
    
    reload: function () {
        var self = this;

        try {
            $.ajax({
                    url: "api/UserCard/Count",
                    success: function(data) {
                        try {
                            if (data == null)
                                return;

                            self.root().find("#notifications").removeClass("badge-important");
                            if (data.NotificationCount > 0)
                                self.root().find("#notifications").addClass("badge-important");

                            self.root().find("#notifications").html(data.NotificationCount);
                            self.root().find("#answers").html(data.AnswerCount);
                            self.root().find("#posts").html(data.PostCount);
                        } catch(e) {
                        }
                    },
                    error: function() { /*override error*/
                }
            });
        }
        catch(e) { }
    }
};


$(postuj).bind("user-authenticated", function () {
    postuj.userCardWidget.init();
});