﻿var postuj = postuj || {};

postuj.comment = function () {
};

$.extend(postuj.comment.prototype, {
    Post: null,
    
    create: function () {
        var self = this;
        $.postData("api/Post/" + this.Post.Id + "/Comment", { Content: self.Content }, function (newComment) {
            var comment = new postuj.comment();
            $.extend(comment, newComment);
            self.Post.CommentsList.push(comment);
            self.Post.Comments++;
            self.Post.render();
            self.Post.setCommentsDisplayed();
        });
    },

    root: function () {
        return $(".comment[dataId=" + this.Id + "]");
    },
    
    render: function (root) {
        var html = $.render["comment"](this, {
            isOwnedByLogedUser: function (comment) {
                return postuj.user.id == comment.Author || postuj.user.isAdmin;
            }
        });

        if (root == null) {
            this.root().html(html);
        }
        else {
            root.append(html);
        }

        this.afterRender();
    },

    afterRender: function () {
        var self = this;

        self.root().find(".delete").click(function() {
            $.deleteData("api/Post/" + self.Post.Id + "/Comment/" + self.Id, null, function () {
                for (var i = 0; self.Post.CommentsList[i].Id != self.Id; i++){
                }

                self.Post.CommentsList.splice(i, 1);
                self.Post.Comments--;
                self.Post.render();
            });
        });
    }
});