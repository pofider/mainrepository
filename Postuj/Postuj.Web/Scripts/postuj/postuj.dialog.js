﻿(function($) {

    $.fn.dialog = function(options) {

        if (!_.isObject(options)) {
            var method = options;
            switch (method) {
                case "hide":
                    $("#modalDialog").find(".close").trigger("click");
            }
        }
        
        $("#modalDialog").modal();
        $("#dialogStorno").show();
        
        $("#dialogHeader").html(options.header);
        $("#dialogContent").html(options.content);
        
        if (options.error) {
            $("#dialogStorno").hide();
        }
      
        $("#dialogSubmit").attr("class", options.submitClass || "  btn btn-danger");
        $("#dialogSubmit").text(options.submitText || "Ano");
        $("#dialogSubmit").unbind("click").click(function () {
            if (options.onSubmit != null)
                options.onSubmit();
            
            $("#modalDialog").modal('hide');
        });
        
        if (options.hideButtons == true) {
            $("#dialogButtons").html("");
        }
        
        if (options.hideSubmit == true) {
            $("#dialogSubmit").hide();
        }
    };
    
    $.extend({
        dialog: $.fn.dialog,
    });
})(jQuery);
