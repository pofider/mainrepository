﻿var postuj = postuj || {};

postuj.post = function () {
    this.init();
};

$.extend(postuj.post.prototype, {
    CommentsList: [],
    CommentsShown: false,
    AnswersVisible: false,
  
    init: function () {
        _.bindAll(this, "toggleComments");
    },

    create: function(context) {
        $.postData("api/Post", this, function (post) {
            context.success(post);
        });
    },
    
    root: function () {
        return $(".post-view[data-id=" + this.Id + "]");
    },

    like: function () {
        if (!this.ensureUserAuthenticated())
            return;

        var self = this;
        $.postData("api/Post/" + this.Id + "/Like", null, function(data) {
            if (data == true) {
                self.Likes++;
                self.render();
            }
        });
    },
    
    dislike: function () {
        if (!this.ensureUserAuthenticated())
            return;

        var self = this;
        $.postData("api/Post/" + this.Id + "/Dislike", null, function (data) {
            if (data == true) {
                self.Dislikes++;
                self.render();
            }
        });
    },
    
    showAnswers: function () {
        var self = this;
      
        if (this.AnswersVisible)
            self.root().find(".answers-area").html("");
        else 
            $.getJSON("api/Post/" + this.Id + "/Answers", function(data) {
                self.root().find(".answers-area").html($.render["answers"]({ answers: data }));
            });
        

        this.AnswersVisible = !this.AnswersVisible;
    },
    
    isOwnedByLogedUser: function(post) {
        return postuj.user.id == post.Author || postuj.user.isAdmin;
    },
   
    render: function (container) {
        var self = this;
        var html = $.render["post"](this, {
            getCssClassForLane: function(post) {
                switch (post.CategoryCode) {
                    case "news": return "lane-green";
                    case "videos": return "lane-orange";
                    case "media": return "lane-purple";
                    case "meetings": return "lane-red";
                    case "events": return "lane-blue";
                }
            },
            isOwnedByLogedUser: this.isOwnedByLogedUser,
        });
        
        if (container == null) {
            try {
                this.root().replaceWith(html);
            }
            catch(ex) { }
        }
        else {
            container.append(html);
        }
        
        _.each(this.CommentsList, function (comment) {
            comment.Post = self;
            comment.render(self.root().find("#commentList"));
        });

        this.setCommentsHided();
        
        this.root().find(".comment-input").hide();

        this.afterRender();
    },
    
    setCommentsDisplayed: function() {
        this.root().find("#showComments").hide();
        this.root().find("#hideComments").show();
    },
    
    setCommentsHided: function() {
        this.root().find("#hideComments").hide();
        this.root().find("#showComments").show();
    },
    
    toggleComments: function (expand) {
        var self = this;
        
        if (this.CommentsShown && !expand) {
            this.setCommentsHided();
        }
        
        if (!this.CommentsShown && expand) {
            this.setCommentsDisplayed();
        }
        
        if (expand)
            this.root().find(".comment-input").show();
        else
            this.root().find(".comment-input").hide();
        
        this.CommentsShown = expand;
        
        if (postuj.user.id == null)
            this.root().find(".comment-input").hide();
        
        this.root().find("#commentList").html("");

        if (expand) {
            $.getJSON("api/Post/" + this.Id + "/Comment", function (data) {
                self.CommentsList = _.map(data, function (c) {
                    var comment = new postuj.comment();
                    $.extend(comment, c);
                    comment.Post = self;
                    return comment;
                });
                
                _.each(self.CommentsList, function(comment) {
                    comment.render(self.root().find("#commentList"));
                });
            });
        } else {
            self.CommentsList = [];
        }
    },
    
    remove: function () {
        if (!this.ensureUserAuthenticated())
            return;

        var self = this;

        $.dialog({
            header: "Smazat příspěvek",
            content: "Opravdu chcete smazat Váš příspěvek?",
            onSubmit: function() {
                $.deleteData("api/Post/" + self.Id, null, function () {
                    $(postuj).trigger("post-deleted", self);
                    postuj.userCardWidget.reload();
                });
            }
        });
    },
    
    answer: function () {
        if (!this.ensureUserAuthenticated())
            return;
        
        var self = this;
        
        $.postData("api/Post/" + self.Id + "/Answer", null, function (data) {
            if (data == true) {
                self.Answers++;
                self.HasMyAnswer = true;
                self.render();
                postuj.userCardWidget.reload();
            }
        });
    },
    
    removeAnswer: function () {
        if (!this.ensureUserAuthenticated())
            return;
        
        var self = this;

        $.deleteData("api/Post/" + self.Id + "/Answer", null, function (data) {
                self.Answers--;
                self.HasMyAnswer = false;
                self.render();
                postuj.userCardWidget.reload();
        });
    },

    afterRender: function () {
        var self = this;

        this.root().find("#like").click(function() {
            self.like();
        });
        
        this.root().find("#dislike").click(function () {
            self.dislike();
        });

        this.root().find("#addComment").click(function () {
            if (self.ensureUserAuthenticated())
                self.toggleComments(true);
        });
        
        this.root().find("#showComments").click(function () {
            self.toggleComments(true);
        });

        this.root().find("#hideComments").click(function () {
            self.toggleComments(false);
        });
        

        this.root().find("#removePost").click(function () {
            self.remove(false);
        });
        
        this.root().find("#answer-command").click(function () {
            self.answer();
        });
        
        this.root().find(".remove-me").click(function () {
            self.removeAnswer();
        });
        
        this.root().find("#attachedImage").click(function () {
            var newtab = window.open();
            newtab.location = self.AttachedImageLink;
        });
        
        this.root().find("#showAnswers").click(function () {
            self.showAnswers();
        });
        
        this.root().find("#videoLazyContent").click(function () {
            $(this).html(self.VideoLink);
        });
        
	    this.root().find("#videoLazyWrap").click(function () {
            $(this).html(self.VideoLink);
	    });
        
        this.root().find("#getLink").click(function () {
            var url = "http://" + window.location.host + "/List/" + self.Id;
            $.dialog({
                header: "Odkaz na příspěvek",
                content: "<a id='postLink' href='"+ url + "' target='_blank'>" + url + "</a>", 
                hideSubmit: true
            });
        });

        this.root().find(".comment-submit").bind("keypress", function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            var input = $(this);
            var len = input.val().length;
            if (len >= 300) 
                input.val(input.val().substring(0, 300));
       
            if (code == 13) {
                var comment = new postuj.comment();
                comment.Content = $(this).val();
                comment.Post = self;
                comment.create();
            }
        });
    },
    
    ensureUserAuthenticated: function() {
        if (postuj.user.id == null) {
            $.dialog({
                header: "Přihlášení",
                content: "Pro tuto operaci musíte být přihlášen.",
                submitText: "Přihlásit přes facebook",
                submitClass: "btn btn-primary",
                onSubmit: function() {
                    $('#loginForm').submit();
                }
            });
            return false;
        }

        return true;
    },
});


