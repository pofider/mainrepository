﻿var postuj = postuj || {};

postuj.list = {
    CurrentList: null,
    Category: null,
    Location: null,
    PageNumber: 1,
    PostedByMe: null,
    AnsweredByMe : null,
    PostId: null,
    OrderBy: null,
    SearchString: null,
    
    initialized: false,
    
    root: function () {
        return $(".posted");
    },

    load: function (avoidRefreshHash) {
        
        if (!postuj.initialized) {
            setTimeout(function () { postuj.list.load(avoidRefreshHash); }, 10);
            return;
        }
        
        var self = this;
        
        this.PageNumber = 1;
        this.getData(function(posts) {
            self.CurrentList = posts;
            self.render();

            if (self.PostedByMe || self.AnsweredByMe)
                postuj.userCardWidget.reload();
        });

        $(postuj).trigger("posts-loaded");

        if (avoidRefreshHash != true)
          this.refreshHash();
    },
    
    reset: function () {
        this.Category = null;
        this.Location = null;
        this.PageNumber = 1;
        this.PostedByMe = null;
        this.AnsweredByMe = null;
        this.PostId = null;
        this.OrderBy = null;
        this.SearchString = null;
    },
    
    loadForNew: function(p) {
        var category = null;
        if (this.Category != p.CategoryCode) {
            category = p.CategoryCode;
        }
        this.reset();
        this.Category = category;

        this.load();
    },
    
    resetUseCardFilters: function() {
        this.PostedByMe = null;
        this.AnsweredByMe = null;
        this.PostId = null;
    },

    setCategory: function(category) {
        this.Category = category;
        this.SearchString = null;
        $(".search-query").val("");
        this.resetUseCardFilters();
    },
    
    setLocation: function (location) {
        this.Location = location;
        this.resetUseCardFilters();
    },
    
    setSearchString: function(searchString) {
        this.setLocation(null);
        this.setCategory(null);
        this.resetUseCardFilters();
        this.SearchString = searchString;
    },
    
    refreshHash: function () {
        var pars = {};
        
        if (this.Category != null)
            pars.Category = this.Category;
        
        if (this.Location != null)
            pars.Location = this.Location;
        
        if (this.PostedByMe != null)
            pars.PostedByMe = this.PostedByMe;
        
        if (this.AnsweredByMe != null)
            pars.AnsweredByMe = this.AnsweredByMe;
        
        if (this.OrderBy != null)
            pars.OrderBy = this.OrderBy;

        if (this.SearchString != null)
            pars.SearchString = this.SearchString;
        
        if (this.PostId != null)
            pars.PostId = this.PostId;
            
        window.location.hash = "/list" + (!_.isEmpty(pars) ? ("?" + $.param(pars)) : "");
    },
    
    getData: function (callback) {
        var self = this;
        
        $(".loader").show();

        $.postData("api/Post/Filter", {
            CategoryCode: self.Category,
            LocationCode: self.Location,
            PageNumber: self.PageNumber,
            PostedByMe: self.PostedByMe,
            AnsweredByMe: self.AnsweredByMe,
            PostId: self.PostId,
            PostOrderCode: self.OrderBy,
            SearchString: self.SearchString
            }, function(data) {
                var posts = _.map(data, function(p) {
                    var post = new postuj.post();
                    $.extend(post, p);

                    post.CommentsList = _.map(post.CommentsList, function(c) {
                        var comment = new postuj.comment();
                        $.extend(comment, c);
                        return comment;
                    });

                    return post;
                });

                callback(posts);
            });
    },
    
    render: function() {
        var self = this;
        
        if (self.root().find("#postList").length == 0) {
            
            self.root().html($.render["post-list"]());

            $.dropdowns();

            self.root().find(".btn-filter").change(function () {
                postuj.list.OrderBy = self.root().find(".btn-filter").val();
                postuj.list.load();
            });
        }

        self.root().find(".main-wall-name h3").html(self.getCategoryString());
        
        self.root().find("#postList").html("");
        
        _.each(self.CurrentList, function (p) {
            p.render($("#postList"));
        });
        
        $("#loadAnotherPage").click(function () {
            $(".loader").show();
            
            self.PageNumber += 1;
            self.getData(function (posts) {
                _.forEach(posts, function (p) {
                    self.CurrentList.push(p);

                    p.render($("#postList"));
                });
                $(".loader").hide();
            });
        });
        
        $(".loader").hide();
        
       
    },
    
    getCategoryString: function () {
        switch(this.Category) {
            case "news" : return "Novinky";
            case "media": return "Obrázky";
            case "events": return "Události / Akce";
            case "meetings": return "Srazy";
            case "videos": return "Videa";
        }

        return "Hlavní zeď";
    },
    
    init: function () {
        var self = this;

        $(postuj).bind("post-deleted", function(e, post) {
            var index = self.CurrentList.indexOf(post);
            self.CurrentList.splice(index, 1);
            self.render();
        });
    }
};

$(postuj).bind("shell-loaded", function () {
    postuj.list.init();
    
    if (window.location.hash == "") {
        postuj.list.load();
    }
});

function mapListPath() {
    if (!postuj.initialized) {
        setTimeout(mapListPath, 20);
        return;
    }

    var changes = false;
    var q = $.getUrlVars();

    if (q.Category != postuj.list.Category)
        changes = true;
    
    if (q.Location != postuj.list.Location)
        changes = true;

    if (q.PageNumber != postuj.list.PageNumber && (q.PageNumber == null && postuj.list.PageNumber != 1))
        changes = true;

    if (q.PostedByMe != postuj.list.PostedByMe)
        changes = true;

    if (q.AnsweredByMe != postuj.list.AnsweredByMe)
        changes = true;

    if (q.PostId != postuj.list.PostId)
        changes = true;

    if (q.OrderBy != postuj.list.OrderBy)
        changes = true;

    if (q.SearchString != postuj.list.SearchString)
        changes = true;

    if (changes || postuj.list.initialized == false) {
        postuj.list.reset();
        $.extend(postuj.list, q);

        postuj.list.load(true);
        postuj.list.initialized = true;
    }
}

Path.map("#/list").to(mapListPath);

Path.listen();
