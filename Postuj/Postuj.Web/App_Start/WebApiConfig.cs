﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Postuj.Infrastructure.DependencyInjection;
using Postuj.Web.Filters;

namespace Postuj.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.DependencyResolver = new AutofacWebApiDependencyResolver(InjectionContainer.Instance);

            config.Filters.Add(new ExceptionFilter());
        }
    }
}
