﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Optimization;
using System.Linq;

namespace Postuj.Web
{
    public class FileNameOrderer : IBundleOrderer
    {
        public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return files.OrderBy(f => f.Name);
        }
    }

    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/common/*.js"));

            var bootstrapBundle = new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/bootstrap/js/*.js");
            bootstrapBundle.Orderer = new FileNameOrderer();
            bundles.Add(bootstrapBundle);
         
            bundles.Add(new ScriptBundle("~/bundles/postuj").Include(
                     "~/Scripts/postuj/*.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/common/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/style.css", 
                "~/Content/bootstrap/css/bootstrap.css"));
            
            foreach (var bundle in bundles)
            {
                bundle.Transforms.Clear();
            }
        }
    }
}