﻿using System.Web.Http.Filters;
using log4net;

namespace Postuj.Web.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute, IExceptionFilter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (ExceptionFilter));

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnException(actionExecutedContext);

            _log.Error(actionExecutedContext.Exception);
        }

    }
}