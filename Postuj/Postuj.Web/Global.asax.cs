﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using NHibernate;
using NHibernate.Linq;
using Postuj.Domain.Domain;
using Postuj.Infrastructure;
using Postuj.Infrastructure.DataAccess.ORM;
using Postuj.Infrastructure.DependencyInjection;
using Postuj.Infrastructure.Logging;
using Postuj.Web.Controllers;
using Postuj.Web.WebServices;

namespace Postuj.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            new OrmConfigure()
                .ForAssemblyType<Post>()
                .UpdateSchema()
                .ConfiguraOrm();
            
            
            ContainerBuilderProvider.Builder.RegisterModule<WebServicesModule>();
            ContainerBuilderProvider.Builder.RegisterModule(new InfrastructureModule());
            ContainerBuilderProvider.Builder.RegisterModule(new ControllersModule());
            InjectionContainer.Instance = ContainerBuilderProvider.Builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(InjectionContainer.Instance));

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            
            log4net.Config.XmlConfigurator.Configure();
            log4net.GlobalContext.Properties["user"] = new HttpContextUserNameProvider(); 
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("cs-CZ");

                string loggedUser = HttpContext.Current.User.Identity.Name;
                var userProfileQuery = DependencyResolver.Current.GetService<ISession>().Query<UserProfile>()
                                                         .Where(u => u.Id.ToString() == loggedUser);

                if (!userProfileQuery.Any()){
                    FormsAuthentication.SignOut();

                    // clear authentication cookie
                    HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                    cookie1.Expires = DateTime.Now.AddYears(-1);
                    Response.Cookies.Add(cookie1);

                    // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
                    HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
                    cookie2.Expires = DateTime.Now.AddYears(-1);
                    Response.Cookies.Add(cookie2);

                    Response.Redirect(Server.MapPath("~/"));

                    return;
                }

                var userProfile = userProfileQuery.Single();

                userProfile.IsAuthenticated = true;
                HttpContext.Current.User = userProfile; 
            }
        }
    }
}